import 'answer.dart';
import 'package:hive/hive.dart';

part 'history.g.dart';

@HiveType(typeId: 4)
class History {
  @HiveField(0)
  String time;

  @HiveField(1)
  Test test;

  @HiveField(2)
  bool select = false;

  @HiveField(3)
  int allAnswer;

  @HiveField(4)
  int correctAnswer;

  @HiveField(5)
  int falseAnswer;

  @HiveField(6)
  bool passed;

  @HiveField(7)
  String nameTest;
  History(
      {required this.falseAnswer,
      required this.nameTest,
      required this.allAnswer,
      required this.correctAnswer,
      required this.passed,
      required this.time,
      required this.test});
}
