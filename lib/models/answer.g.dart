// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'answer.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class QuestionsAdapter extends TypeAdapter<Questions> {
  @override
  final int typeId = 5;

  @override
  Questions read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Questions(
      id: fields[0] as int,
      name: fields[1] as String,
      tests: (fields[2] as Map).map((dynamic k, dynamic v) =>
          MapEntry(k as String, (v as List).cast<Test>())),
    );
  }

  @override
  void write(BinaryWriter writer, Questions obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.tests);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QuestionsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class TestAdapter extends TypeAdapter<Test> {
  @override
  final int typeId = 1;

  @override
  Test read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Test(
      selectQuestion: fields[0] as int,
      testEnd: fields[2] as bool,
      testStart: fields[3] as bool,
      bestResult: fields[1] as int,
      id: fields[4] as String,
      name: fields[5] as String,
      questions: (fields[6] as List).cast<Question>(),
    );
  }

  @override
  void write(BinaryWriter writer, Test obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.selectQuestion)
      ..writeByte(1)
      ..write(obj.bestResult)
      ..writeByte(2)
      ..write(obj.testEnd)
      ..writeByte(3)
      ..write(obj.testStart)
      ..writeByte(4)
      ..write(obj.id)
      ..writeByte(5)
      ..write(obj.name)
      ..writeByte(6)
      ..write(obj.questions);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TestAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class QuestionAdapter extends TypeAdapter<Question> {
  @override
  final int typeId = 2;

  @override
  Question read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Question(
      editExam: fields[0] as bool,
      colorContainer:
          Color(int.parse(fields[2].split('(0x')[1].split(')')[0], radix: 16)),
      colorText:
          Color(int.parse(fields[1].split('(0x')[1].split(')')[0], radix: 16)),
      correntAnswerId: fields[4] as int,
      id: fields[5] as String,
      title: fields[6] as String,
      answers: (fields[7] as List).cast<Answer>(),
      image: fields[8] as String,
      selectAnswerId: fields[3] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Question obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.editExam)
      ..writeByte(1)
      ..write(obj.colorText.toString())
      ..writeByte(2)
      ..write(obj.colorContainer.toString())
      ..writeByte(3)
      ..write(obj.selectAnswerId)
      ..writeByte(4)
      ..write(obj.correntAnswerId)
      ..writeByte(5)
      ..write(obj.id)
      ..writeByte(6)
      ..write(obj.title)
      ..writeByte(7)
      ..write(obj.answers)
      ..writeByte(8)
      ..write(obj.image);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QuestionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AnswerAdapter extends TypeAdapter<Answer> {
  @override
  final int typeId = 3;

  @override
  Answer read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Answer(
      id: fields[0] as int,
      text: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Answer obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.text);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AnswerAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
