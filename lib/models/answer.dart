// To parse this JSON data, do
//
// final questions = questionsFromJson(jsonString);

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'dart:convert';
import 'package:hive/hive.dart';

part 'answer.g.dart';

Questions questionsFromJson(String str) => Questions.fromJson(json.decode(str));

String questionsToJson(Questions data) => json.encode(data.toJson());

@HiveType(typeId: 5)
class Questions {
  Questions({
    required this.id,
    required this.name,
    required this.tests,
  });
  @HiveField(0)
  int id;
  @HiveField(1)
  String name;
  @HiveField(2)
  Map<String, List<Test>> tests;

  factory Questions.fromJson(Map<String, dynamic> json) => Questions(
        id: json["id"],
        name: json["name"],
        tests: Map.from(json["tests"]).map((k, v) =>
            MapEntry<String, List<Test>>(
                k, List<Test>.from(v.map((x) => Test.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "tests": Map.from(tests).map((k, v) => MapEntry<String, dynamic>(
            k, List<dynamic>.from(v.map((x) => x.toJson())))),
      };
}

@HiveType(typeId: 1)
class Test {
  Test({
    this.selectQuestion = 0,
    this.testEnd = false,
    this.testStart = false,
    this.bestResult = 0,
    required this.id,
    required this.name,
    required this.questions,
  });
  @HiveField(0)
  int selectQuestion;
  @HiveField(1)
  int bestResult;
  @HiveField(2)
  bool testEnd;
  @HiveField(3)
  bool testStart;
  @HiveField(4)
  String id;
  @HiveField(5)
  String name;
  @HiveField(6)
  List<Question> questions;

  factory Test.clearTest(Test test) {
    List<Question> quest = [];
    for (int i = 0; i < test.questions.length; i++) {
      quest.add(Question.copyWith(test.questions[i]));
    }

    return Test(
        id: test.id,
        name: test.name,
        bestResult: test.bestResult,
        questions: quest);
  }

  factory Test.copyWith(Test test) {
    List<Question> quest = [];
    for (int i = 0; i < test.questions.length; i++) {
      quest.add(Question.copyWithEqual(test.questions[i]));
    }

    return Test(
        id: "1",
        name: test.name,
        bestResult: test.bestResult,
        questions: quest);
  }

  factory Test.fromJson(Map<String, dynamic> json) => Test(
        id: json["id"],
        name: json["name"],
        questions: List<Question>.from(
            json["questions"].map((x) => Question.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "questions": List<dynamic>.from(questions.map((x) => x.toJson())),
      };
}

@HiveType(typeId: 2)
class Question {
  Question({
    this.editExam = true,
    this.colorContainer = Colors.white,
    this.colorText = const Color.fromRGBO(0, 0, 0, 0.4),
    required this.correntAnswerId,
    required this.id,
    required this.title,
    required this.answers,
    required this.image,
    required this.selectAnswerId,
  });
  @HiveField(0)
  bool editExam;
  @HiveField(1)
  Color colorText;
  @HiveField(2)
  Color colorContainer;
  @HiveField(3)
  int selectAnswerId;
  @HiveField(4)
  int correntAnswerId;
  @HiveField(5)
  String id;
  @HiveField(6)
  String title;
  @HiveField(7)
  List<Answer> answers;
  @HiveField(8)
  String image;

  factory Question.copyWithEqual(Question question) => Question(
      correntAnswerId: question.correntAnswerId,
      id: question.id,
      title: question.title,
      answers: question.answers,
      image: question.image,
      colorContainer: question.colorContainer,
      colorText: question.colorText,
      editExam: question.editExam,
      selectAnswerId: question.selectAnswerId);

  factory Question.copyWith(Question question) => Question(
      correntAnswerId: question.correntAnswerId,
      id: question.id,
      title: question.title,
      answers: question.answers,
      image: question.image,
      selectAnswerId: -1);

  factory Question.fromJson(Map<String, dynamic> json) => Question(
        correntAnswerId: json["correntAnswerID"],
        id: json["id"],
        title: json["title"],
        selectAnswerId: -1,
        answers:
            List<Answer>.from(json["answers"].map((x) => Answer.fromJson(x))),
        image: json["image"] == null ? "" : json["image"],
      );

  Map<String, dynamic> toJson() => {
        "correntAnswerID": correntAnswerId,
        "id": id,
        "title": title,
        "answers": List<dynamic>.from(answers.map((x) => x.toJson())),
        "image": image == null ? "" : image,
      };
}

@HiveType(typeId: 3)
class Answer {
  Answer({
    required this.id,
    required this.text,
  });
  @HiveField(0)
  int id;
  @HiveField(1)
  String text;

  factory Answer.fromJson(Map<String, dynamic> json) => Answer(
        id: json["id"],
        text: json["text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "text": text,
      };
}
