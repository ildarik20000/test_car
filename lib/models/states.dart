import 'package:hive/hive.dart';

part 'states.g.dart';

@HiveType(typeId: 0)
class StateModel {
  @HiveField(0)
  String state;
  @HiveField(1)
  int question;
  @HiveField(2)
  int correctAnswers;
  StateModel(this.state, this.question, this.correctAnswers);
}

List<StateModel> states = [
  StateModel("Alabama", 40, 32),
  StateModel("Alaska", 20, 16),
  StateModel("Arizona", 30, 24),
  StateModel("Arkansas", 25, 20),
  StateModel("California", 36, 30),
  StateModel("Colorado", 25, 20),
  StateModel("Connecticut", 25, 20),
  StateModel("Delaware", 30, 24),
  StateModel("District of Columbia", 25, 20),
  StateModel("Florida", 50, 40),
  StateModel("Georgia", 40, 30),
  StateModel("Hawaii", 30, 24),
  StateModel("Idaho", 40, 34),
  StateModel("Illinois", 35, 28),
  StateModel("Indiana", 50, 42),
  StateModel("Iowa", 35, 28),
  StateModel("Kansas", 25, 20),
  StateModel("Kentucky", 40, 32),
  StateModel("Louisiana", 40, 32),
  StateModel("Maine", 60, 48),
  StateModel("Maryland", 25, 22),
  StateModel("Massachusetts", 25, 18),
  StateModel("Michigan", 50, 40),
  StateModel("Minnesota", 40, 32),
  StateModel("Mississippi", 30, 24),
  StateModel("Missouri", 25, 20),
  StateModel("Montana", 33, 27),
  StateModel("Nebraska", 25, 20),
  StateModel("Nevada", 50, 40),
  StateModel("New Hampshire", 40, 32),
  StateModel("New Jersey", 50, 40),
  StateModel("New Mexico", 25, 18),
  StateModel("New York", 20, 14),
  StateModel("North Carolina", 37, 29),
  StateModel("North Dakota", 25, 20),
  StateModel("Ohio", 40, 30),
  StateModel("Oklahoma", 25, 20),
  StateModel("Oregon", 35, 28),
  StateModel("Pennsylvania", 18, 15),
  StateModel("Rhode Island", 25, 20),
  StateModel("South Carolina", 30, 24),
  StateModel("South Dakota", 25, 20),
  StateModel("Tennessee", 30, 24),
  StateModel("Texas", 30, 21),
  StateModel("Utah", 25, 20),
  StateModel("Vermont", 20, 16),
  StateModel("Virginia", 40, 34),
  StateModel("Washington", 40, 32),
  StateModel("West Virginia", 25, 19),
  StateModel("Wisconsin", 50, 40),
  StateModel("Wyoming", 50, 40),
];
