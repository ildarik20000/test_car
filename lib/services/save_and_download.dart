import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/controllers/history_controller.dart';
import 'package:dmv_test/models/history.dart';
import 'package:get/get.dart';

import '../models/answer.dart';
import 'boxes.dart';

saveAllInfo() {
  final answerController = Get.put(AnswerController());
  final historyController = Get.put(HistoryController());
  print("save");
  Boxes.getTest().clear();
  Boxes.getHistory().clear();
  Boxes.getTest().addAll(answerController.allTest);
  Boxes.getHistory().addAll(historyController.historys);
}

downloadAllInfo() {
  final answerController = Get.put(AnswerController());
  final historyController = Get.put(HistoryController());
  print("COPY");

  if (Boxes.getTest().values.toList().cast<Test>().isNotEmpty)
    answerController.allTest = Boxes.getTest().values.toList().cast<Test>();
  if (Boxes.getHistory().values.toList().cast<History>().isNotEmpty)
    historyController.historys =
        Boxes.getHistory().values.toList().cast<History>();
  // historyController.getHistoryWidget();
}
