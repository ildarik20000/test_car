import 'package:dmv_test/models/history.dart';
import 'package:dmv_test/models/states.dart';
import 'package:hive/hive.dart';

import '../models/answer.dart';

class Boxes {
  static Box<StateModel> getState() => Hive.box<StateModel>('state');
  static Box<Test> getTest() => Hive.box<Test>('allTest');
  static Box<History> getHistory() => Hive.box<History>('history');
}
