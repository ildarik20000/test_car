

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:share/share.dart';

import '../main.dart';
final browser = ChromeSafariBrowser();
supportFormPressed() {
  _open(support);
}

sharePressed() {
  Share.share('http://apps.apple.com/US/app/id$appID');
}

privacyPolicyPressed() {
  _open(pp);
}

termsOfUsePressed() {
  _open(to);
}

_open(String url) {
  browser.open(
      url: Uri.parse(url),
      options: ChromeSafariBrowserClassOptions(
          android: AndroidChromeCustomTabsOptions(
              addDefaultShareMenuItem: false, keepAliveEnabled: true),
          ios: IOSSafariOptions(
              dismissButtonStyle: IOSSafariDismissButtonStyle.CLOSE,
              presentationStyle:
                  IOSUIModalPresentationStyle.OVER_FULL_SCREEN)));
}