import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  Widget left;
  Widget right;
  Widget center;

  CustomAppBar(this.left, this.center, this.right);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: 40,
          color: Colors.white,
        ),
        Container(
          padding: EdgeInsets.all(16),
          height: 62,
          width: double.infinity,
          color: Colors.white,
          child: Column(
            children: [
              Stack(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      child: left,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      child: center,
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      child: right,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
