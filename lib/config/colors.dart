import 'package:flutter/material.dart';

class AppColors {
  static final black = Color.fromRGBO(0, 0, 0, 0.9);
  static final gray = Color.fromRGBO(0, 0, 0, 0.4);
  static final purple = Color.fromRGBO(97, 70, 198, 1);
  static final backgroundColor = Color.fromRGBO(245, 246, 251, 1);
  static final red = Color.fromRGBO(237, 108, 95, 1);
  static final success = Color.fromRGBO(97, 200, 119, 1);
  static final yellow = Color.fromRGBO(244, 185, 107, 1);
}
