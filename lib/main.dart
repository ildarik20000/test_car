import 'dart:io';

import 'package:apphud/apphud.dart';
import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/controllers/settings_controller.dart';
import 'package:dmv_test/models/states.dart';
import 'package:dmv_test/pages/main_page.dart';
import 'package:dmv_test/pages/onboarding/onboarding_page1.dart';
import 'package:dmv_test/pages/splash_screen.dart';
import 'package:dmv_test/pages/subscription_page.dart';
import 'package:dmv_test/services/boxes.dart';
import 'package:dmv_test/services/save_and_download.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'models/answer.dart';
import 'models/history.dart';

bool download = true;

final appID = '1588554686';
//final appID = '1597471260'; //example
final to =
    'https://docs.google.com/document/d/1mQ-KZmJ7uPnc8yXo903e5WRJbLi_QJPKXvuPo1U1QN4/edit?usp=sharing';
final pp =
    'https://docs.google.com/document/d/15w_XfG13KTx-Ac8OXZLB6RL24x3IHjY_NCzZsl2aDqY/edit?usp=sharing';
final support = 'https://forms.gle/j9AhsYM53iUSNGWV8';
//final appHudKey = 'app_7e6qx2HTEwabX974snxcmoYLgvD3mJ'; //example
final appHudKey = 'app_is1sxdxj4U3mAjL5Coj3LcLv8Xzedm';

bool subscribe = false;

List<String> subs = ["One", "Two"];
void main() async {
  await Hive.initFlutter();
  await Hive.openBox('settings');
  Hive.registerAdapter(StateModelAdapter());
  Hive.registerAdapter(HistoryAdapter());
  Hive.registerAdapter(TestAdapter());
  Hive.registerAdapter(QuestionAdapter());
  Hive.registerAdapter(AnswerAdapter());
  Hive.registerAdapter(QuestionsAdapter());
  await Hive.openBox<StateModel>('state');
  await Hive.openBox<History>('history');
  await Hive.openBox<Test>('allTest');
  await Apphud.start(apiKey: appHudKey);
  subscribe = await Apphud.hasActiveSubscription();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final settingsController = Get.put(SettingsController());
  final answerController = Get.put(AnswerController());
  @override
  void initState() {
    // TODO: implement initState
    answerController.subscribe.value = subscribe;
    super.initState();
    settingsController.state = Boxes.getState().values.isNotEmpty
        ? Boxes.getState().values.last
        : StateModel("Alabama", 40, 32);
    answerController.updateAllTests();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return GetMaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final answerController = Get.put(AnswerController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AnswerController>(builder: (_) {
      if (answerController.allTest.isNotEmpty && download) {
        download = false;
        downloadAllInfo();
      }
      return SplashScreen();
    });
  }
}
