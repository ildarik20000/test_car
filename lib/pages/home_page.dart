import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/pages/manual_page.dart';
import 'package:dmv_test/pages/practice_page.dart';
import 'package:dmv_test/pages/subscription_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

import '../services/save_and_download.dart';
import 'exam_page.dart';

class HomePage extends StatelessWidget {
  final answerController = Get.put(AnswerController());

  HomePage({Key? key}) : super(key: key);
  // int valueSlider = 29;

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => GetBuilder<AnswerController>(builder: (_) {
              return Container(
                child: Column(
                  children: [
                    _appBarContainer(),
                    SizedBox(
                      height: 24,
                    ),
                    Expanded(
                      child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                _manualCard(),
                                SizedBox(
                                  height: 16,
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: InkWell(
                                      onTap: () {
                                        Get.to(() => PracticePage());
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(16),
                                        alignment: Alignment.topLeft,
                                        height: 132,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(16),
                                            color: Colors.white),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Image.asset(
                                              "assets/images/practice.png",
                                              width: 36,
                                            ),
                                            SizedBox(
                                              height: 16,
                                            ),
                                            Text(
                                              "Practice",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 24.sp,
                                                  color: AppColors.black),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "20 tests",
                                                  style: GoogleFonts.inter(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 11.sp,
                                                      color: AppColors.gray),
                                                ),
                                                SizedBox(
                                                  width: 4,
                                                ),
                                                Container(
                                                  width: 4,
                                                  height: 4,
                                                  decoration: BoxDecoration(
                                                      color: AppColors.purple,
                                                      shape: BoxShape.circle),
                                                ),
                                                SizedBox(
                                                  width: 4,
                                                ),
                                                Text(
                                                  "40 QA each",
                                                  style: GoogleFonts.inter(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 11.sp,
                                                      color: AppColors.gray),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    )),
                                    SizedBox(
                                      width: 16,
                                    ),
                                    Expanded(
                                        child: InkWell(
                                      onTap: () {
                                        if (answerController.subscribe.value ||
                                            Hive.box('settings').get('tapExam',
                                                    defaultValue: 0) <
                                                5) {
                                          Get.to(() => ExamPage());
                                        } else
                                          Get.to(() => SubscriptionPage());

                                        Hive.box('settings').put(
                                            'tapExam',
                                            Hive.box('settings').get('tapExam',
                                                    defaultValue: 0) +
                                                1);
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(16),
                                        alignment: Alignment.topLeft,
                                        height: 132,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(16),
                                            color: Colors.white),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Image.asset(
                                              "assets/images/practice.png",
                                              width: 36,
                                            ),
                                            SizedBox(
                                              height: 16,
                                            ),
                                            Text(
                                              "Exam",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 24.sp,
                                                  color: AppColors.black),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "20 min",
                                                  style: GoogleFonts.inter(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 11.sp,
                                                      color: AppColors.gray),
                                                ),
                                                SizedBox(
                                                  width: 4,
                                                ),
                                                Container(
                                                  width: 4,
                                                  height: 4,
                                                  decoration: BoxDecoration(
                                                      color: AppColors.purple,
                                                      shape: BoxShape.circle),
                                                ),
                                                SizedBox(
                                                  width: 4,
                                                ),
                                                Text(
                                                  "40 QA",
                                                  style: GoogleFonts.inter(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 11.sp,
                                                      color: AppColors.gray),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    )),
                                  ],
                                )
                              ],
                            ),
                          )),
                    )
                  ],
                ),
              );
            }));
  }

  Widget _manualCard() {
    return InkWell(
      onTap: () {
        Get.to(() => ManualPage());
      },
      child: Container(
        height: 132,
        width: 343,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.all(16),
              height: 132,
              width: 343,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.white,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    "assets/images/manual.png",
                    width: 36,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    "Manual",
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 24,
                        color: AppColors.black),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: [
                      Text(
                        "DMV Driver's Manual",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                            color: AppColors.gray),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Container(
                        width: 4,
                        height: 4,
                        decoration: BoxDecoration(
                            color: AppColors.purple, shape: BoxShape.circle),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        "60 PG",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                            color: AppColors.gray),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(right: 16),
                child: Image.asset("assets/images/backgroundCardImage.png")),
          ],
        ),
      ),
    );
  }

  Widget _appBarContainer() {
    final slider = SleekCircularSlider(
      appearance: CircularSliderAppearance(
          infoProperties: InfoProperties(
              mainLabelStyle: TextStyle(color: Color.fromRGBO(0, 0, 0, 0))),
          startAngle: -90,
          angleRange: 360,
          customColors: CustomSliderColors(
              dotColor: Color.fromRGBO(0, 0, 0, 0),
              // dynamicGradient: true,
              trackGradientStartAngle: 0,
              trackGradientEndAngle: 0,
              progressBarColors: [
                AppColors.purple,
                Color.fromRGBO(223, 218, 244, 1),
              ],
              trackColor: Color.fromRGBO(240, 240, 240, 1)),
          customWidths: CustomSliderWidths(
              shadowWidth: 0,
              trackWidth: 15,
              progressBarWidth: (answerController.getQuestionsCompleted() /
                          answerController.getAllQuestions() *
                          100) >
                      0
                  ? 15
                  : 0)),
      min: 0,
      max: 100,
      initialValue: answerController.getQuestionsCompleted() /
          answerController.getAllQuestions() *
          100,
    );
    return Container(
      height: 244,
      child: Stack(
        children: [
          Container(
            height: 220,
            width: double.infinity,
            padding: EdgeInsets.only(
              left: 34.h,
              top: 20,
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(36),
                    bottomRight: Radius.circular(36)),
                color: Colors.white),
            child: Row(
              children: [
                SizedBox(height: 120, width: 120, child: Center(child: slider)),
                SizedBox(
                  width: 16,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 45,
                    ),
                    Text(
                      "You score",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w600,
                          fontSize: 24.sp,
                          color: AppColors.black),
                    ),
                    Text(
                      (answerController.getQuestionsCompleted() /
                                  answerController.getAllQuestions() *
                                  100)
                              .toInt()
                              .toString() +
                          "%",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w600,
                          fontSize: 48.sp,
                          color: AppColors.purple),
                    ),
                    Text(
                      "${answerController.getQuestionsCompleted()}/${answerController.getAllQuestions()} questions completed",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w500,
                          fontSize: 12.sp,
                          color: AppColors.gray),
                    ),
                  ],
                )
              ],
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: Hive.box("settings").get("typeLicense") == 0 ? 165 : 232,
                height: 52,
                padding: EdgeInsets.symmetric(horizontal: 14, vertical: 11.5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(23),
                    color: Colors.white,
                    border: Border.all(
                        width: 6, color: Color.fromRGBO(245, 246, 251, 1))),
                child: Center(
                  child: Text(
                    Hive.box("settings").get("typeLicense") == 0
                        ? "CAR LICENSE"
                        : "MOTORCYCLE LICENSE",
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 16.sp,
                        color: AppColors.black),
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
