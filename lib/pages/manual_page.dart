import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:webview_flutter/webview_flutter.dart';

import '../config/colors.dart';
import '../controllers/settings_controller.dart';
import '../shared/appbar.dart';

class ManualPage extends StatefulWidget {
  @override
  State<ManualPage> createState() => _ManualPageState();
}

class _ManualPageState extends State<ManualPage> {
  final settingsController = Get.put(SettingsController());
  late String url;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    url =
        "https://dmv-permit-test.com/js/pdfjs/handbook/${settingsController.state.state.toLowerCase().replaceAll(RegExp(r' '), "-")}.pdf";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            bottom: false,
            top: false,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              CustomAppBar(
                  InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      child: Image.asset(
                        "assets/images/Back.png",
                        width: 24,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 37),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Manual",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: AppColors.black),
                    ),
                  ),
                  Container(
                    child: InkWell(
                      onTap: () {
                        FlutterShare.share(
                          title: 'Manual',
                          // text: 'Manual',
                          linkUrl: url,
                          // chooserTitle: 'Manual'
                        );
                      },
                      child: Image.asset(
                        "assets/images/ShareManual.png",
                        width: 24,
                      ),
                    ),
                  )),
              Expanded(
                  child: WebView(
                initialUrl: url,
              ))
            ])));
  }
}
