import 'package:app_review/app_review.dart';
import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/pages/onboarding/onboarding_page3.dart';
import 'package:dmv_test/shared/background_onboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class OnboardingPage2 extends StatefulWidget {
  @override
  State<OnboardingPage2> createState() => _OnboardingPage2State();
}

class _OnboardingPage2State extends State<OnboardingPage2> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => Scaffold(
              body: Stack(children: [
                backgroundOnboard(),
                SafeArea(
                  bottom: false,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          // color: Colors.purple,
                          child: Image.asset(
                        "assets/images/onboarding2.png",
                        fit: BoxFit.fitWidth,
                        height: 414.h,
                      )),
                      SizedBox(
                        height: 40.h,
                      ),
                      Text(
                        "Help us\nbecome better",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.inter(
                            color: AppColors.black,
                            fontSize: 36.sp,
                            fontWeight: FontWeight.w800),
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 56.0),
                        child: Text(
                          "Please let us know. We are loking for any feedback",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.inter(
                              color: AppColors.gray,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(
                        height: 42.h,
                      ),
                      GestureDetector(
                        onTap: () {
                          Get.offAll(() => OnboardingPage3());
                        },
                        child: Image.asset(
                          "assets/images/onboardingButton2.png",
                          width: 76.w,
                        ),
                      ),
                      // SizedBox(
                      //   height: 40.h,
                      // ),
                      // SizedBox(
                      //   height: 35.h,
                      // )
                    ],
                  ),
                )
              ]),
            ));
  }
}
