import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/settings_controller.dart';
import 'package:dmv_test/models/states.dart';
import 'package:dmv_test/pages/main_page.dart';
import 'package:dmv_test/pages/subscription_page.dart';
import 'package:dmv_test/services/boxes.dart';
import 'package:dmv_test/services/browser_services.dart';
import 'package:dmv_test/shared/background_onboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';

class OnboardingPage4 extends StatefulWidget {
  int tap = -1;
  @override
  State<OnboardingPage4> createState() => _OnboardingPage4State();
}

class _OnboardingPage4State extends State<OnboardingPage4> {
  List<StateModel> searcList = [];

  @override
  void initState() {
    // TODO: implement initState
    searcList.addAll(states);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => GestureDetector(
              onTap: () {
                setState(() {
                  FocusManager.instance.primaryFocus?.unfocus();
                });
              },
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                body: Stack(children: [
                  backgroundOnboard(),
                  SafeArea(
                    bottom: false,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(
                          height: 20.h,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 38),
                          child: Text(
                            "Choose your state",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.inter(
                                color: AppColors.black,
                                fontSize: 36.sp,
                                fontWeight: FontWeight.w800),
                          ),
                        ),
                        SizedBox(
                          height: 32.h,
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 24),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.1),
                                      offset: Offset(0, -5),
                                      blurRadius: 15)
                                ],
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(24),
                                    topRight: Radius.circular(24))),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 16.h,
                                ),
                                Container(
                                  height: 48,
                                  width: double.infinity,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 14),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: Color.fromRGBO(240, 240, 240, 1)),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: TextField(
                                        onChanged: (query) {
                                          filterSearchResults(query);
                                        },
                                        cursorColor: AppColors.purple,
                                        decoration: InputDecoration(
                                            contentPadding: EdgeInsets.only(
                                                top: 0, bottom: 10),
                                            hintText: "Search",
                                            hintStyle: GoogleFonts.inter(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 16,
                                                color: AppColors.gray),
                                            border: InputBorder.none),
                                      )),
                                      Image.asset(
                                        "assets/images/search.png",
                                        width: 24,
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 24.h,
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Available states",
                                    textAlign: TextAlign.start,
                                    style: GoogleFonts.inter(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14.sp,
                                        color: AppColors.gray),
                                  ),
                                ),
                                SizedBox(
                                  height: 12.h,
                                ),
                                Expanded(
                                  child: searcList.isNotEmpty
                                      ? ListView.builder(
                                          itemCount: searcList.length,
                                          itemBuilder: (context, index) {
                                            return InkWell(
                                              onTap: () {
                                                setState(() {
                                                  widget.tap = index;
                                                });
                                                final settingsController =
                                                    Get.put(
                                                        SettingsController());
                                                settingsController.state =
                                                    searcList[widget.tap];
                                                Boxes.getState()
                                                    .add(searcList[widget.tap]);
                                              },
                                              child: _statesElement(
                                                  searcList[index].state,
                                                  widget.tap == index),
                                            );
                                          })
                                      : Center(
                                          child: Text(
                                            "Not found state",
                                            style: GoogleFonts.inter(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16,
                                                color: AppColors.gray),
                                          ),
                                        ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        if (widget.tap != -1) {
                                          Hive.box('settings')
                                              .put('showOnboarding', true);

                                          Get.offAll(() => MainPage());
                                          Get.to(() => SubscriptionPage());
                                        }
                                      },
                                      child: Image.asset(
                                        "assets/images/onboardingButton4${widget.tap == -1 ? "_disabled" : ""}.png",
                                        width: 76.w,
                                      ),
                                    ),
                                    Container(
                                      height: 40.h,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          InkWell(
                                            onTap: termsOfUsePressed,
                                            child: Text(
                                              "Term of Use",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14,
                                                  color: AppColors.gray),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: privacyPolicyPressed,
                                            child: Text(
                                              "Privacy Policy",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14,
                                                  color: AppColors.gray),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 35.h,
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ]),
              ),
            ));
  }

  Widget _statesElement(String text, bool enabled) {
    return Container(
      height: 48,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text,
            style: GoogleFonts.inter(
                fontWeight: FontWeight.w500,
                fontSize: 16,
                color: AppColors.black),
          ),
          Container(
            width: 20,
            height: 20,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: enabled
                    ? Color.fromRGBO(219, 212, 241, 1)
                    : Color.fromRGBO(0, 0, 0, 0.1)),
            child: enabled
                ? Center(
                    child: Image.asset(
                    "assets/images/onboarding3_3.png",
                    color: AppColors.purple,
                    width: 10,
                  ))
                : Container(),
          )
        ],
      ),
    );
  }

  void filterSearchResults(String query) {
    List<StateModel> dummySearchList = [];
    dummySearchList.addAll(states);
    if (query.isNotEmpty) {
      List<StateModel> dummyListData = [];
      dummySearchList.forEach((item) {
        if (item.state.toLowerCase().startsWith(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        searcList.clear();
        searcList.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        searcList.clear();
        searcList.addAll(states);
      });
    }
  }
}
