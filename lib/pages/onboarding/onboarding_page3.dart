import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/pages/onboarding/onboarding_page4.dart';
import 'package:dmv_test/shared/background_onboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';

class OnboardingPage3 extends StatefulWidget {
  int tap = -1;
  @override
  State<OnboardingPage3> createState() => _OnboardingPage3State();
}

class _OnboardingPage3State extends State<OnboardingPage3> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => Scaffold(
              body: Stack(children: [
                backgroundOnboard(),
                SafeArea(
                  bottom: false,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 20.h,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 38),
                        child: Text(
                          "What license do you need?",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.inter(
                              color: AppColors.black,
                              fontSize: 36.sp,
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      SizedBox(
                        height: 69.h,
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.1),
                                    offset: Offset(0, -5),
                                    blurRadius: 15)
                              ],
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(24),
                                  topRight: Radius.circular(24))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 16.h,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 56.0),
                                child: Text(
                                  "Please select the type of license you need",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.inter(
                                      color: AppColors.gray.withOpacity(0.25),
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              SizedBox(
                                height: 36.h,
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    widget.tap = 0;
                                  });
                                },
                                child: AnimatedContainer(
                                  duration: Duration(milliseconds: 300),
                                  height: 132.w,
                                  width: double.infinity,
                                  margin: EdgeInsets.symmetric(horizontal: 24),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: widget.tap == 0
                                          ? Color.fromRGBO(219, 212, 241, 1)
                                          : Color.fromRGBO(240, 240, 240, 1)),
                                  child: Stack(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 19, top: 19.w, bottom: 19.w),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              width: 30.w,
                                              height: 30.w,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(30),
                                                  color: widget.tap == 0
                                                      ? AppColors.purple
                                                      : Colors.white),
                                              child: Center(
                                                child: Image.asset(
                                                  "assets/images/onboarding3_3.png",
                                                  width: 15.w,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 19.h,
                                            ),
                                            Text(
                                              "Car",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 24.sp),
                                            ),
                                            Text(
                                              "Driver’s license or learner’s permit",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12.sp,
                                                  color: AppColors.gray),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: Image.asset(
                                          "assets/images/onboarding3_1${widget.tap == 0 ? "_enabled" : ""}.png",
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 16.h,
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    widget.tap = 1;
                                  });
                                },
                                child: AnimatedContainer(
                                  duration: Duration(milliseconds: 300),
                                  height: 132.w,
                                  width: double.infinity,
                                  margin: EdgeInsets.symmetric(horizontal: 24),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: widget.tap == 1
                                          ? Color.fromRGBO(219, 212, 241, 1)
                                          : Color.fromRGBO(240, 240, 240, 1)),
                                  child: Stack(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 19, top: 19.w, bottom: 19.w),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              width: 30.w,
                                              height: 30.w,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(30),
                                                  color: widget.tap == 1
                                                      ? AppColors.purple
                                                      : Colors.white),
                                              child: Center(
                                                child: Image.asset(
                                                  "assets/images/onboarding3_3.png",
                                                  width: 15.w,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 19.h,
                                            ),
                                            Text(
                                              "Motorcycle",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 24.sp),
                                            ),
                                            Text(
                                              "Motorcycle rider’s license or permit",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12.sp,
                                                  color: AppColors.gray),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: Image.asset(
                                          "assets/images/onboarding3_2${widget.tap == 1 ? "_enabled" : ""}.png",
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height > 800
                                    ? 92.h
                                    : 26.h,
                              ),
                              GestureDetector(
                                onTap: () {
                                  if (widget.tap != -1) {
                                    Hive.box('settings')
                                        .put('typeLicense', widget.tap);
                                    Get.offAll(() => OnboardingPage4());
                                  }
                                },
                                child: Image.asset(
                                  "assets/images/onboardingButton3${widget.tap == -1 ? "_disabled" : ""}.png",
                                  width: 76.w,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ]),
            ));
  }
}
