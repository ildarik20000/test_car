import 'package:app_review/app_review.dart';
import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/pages/onboarding/onboarding_page2.dart';
import 'package:dmv_test/shared/background_onboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class OnboardingPage1 extends StatelessWidget {
  const OnboardingPage1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => Scaffold(
              body: Stack(children: [
                backgroundOnboard(),
                SafeArea(
                  bottom: false,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 90.h,
                      ),
                      Container(
                          // color: Colors.purple,
                          child: Image.asset(
                        "assets/images/onboarding1.png",
                        height: 330.h,
                      )),
                      SizedBox(
                        height: 40.h,
                      ),
                      Text(
                        "DMV\nPractice Test",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.inter(
                            color: AppColors.black,
                            fontSize: 36.sp,
                            fontWeight: FontWeight.w800),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 56.0),
                        child: Text(
                          "Test is based on the DMV Manual for your state",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.inter(
                              color: AppColors.gray,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      SizedBox(
                        height: 42.h,
                      ),
                      GestureDetector(
                        onTap: () async {
                          await AppReview.requestReview;
                          Get.offAll(() => OnboardingPage2());
                        },
                        child: Image.asset(
                          "assets/images/onboardingButton1.png",
                          width: 76.w,
                        ),
                      ),
                      // SizedBox(
                      //   height: 40.h,
                      // ),
                      // SizedBox(
                      //   height: 35.h,
                      // )
                    ],
                  ),
                )
              ]),
            ));
  }
}
