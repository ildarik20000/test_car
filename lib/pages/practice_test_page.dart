import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/timer_controller.dart';
import 'package:dmv_test/pages/practice_result_page.dart';

import 'package:dmv_test/shared/appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/answer_controller.dart';
import '../models/answer.dart';

class PracticeTestPage extends StatefulWidget {
  Test test;
  double scrollPozition = 0;
  PracticeTestPage(this.test, {Key? key}) : super(key: key);

  @override
  State<PracticeTestPage> createState() => _PracticeTestPageState();
}

class _PracticeTestPageState extends State<PracticeTestPage> {
  late ScrollController scrollController;

  @override
  void initState() {
    super.initState();

    widget.scrollPozition = widget.test.selectQuestion * 63;
    scrollController =
        ScrollController(initialScrollOffset: widget.scrollPozition);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: SafeArea(
        top: false,
        child: Column(
          children: [
            CustomAppBar(
                InkWell(
                  onTap: () {
                    showCupertinoDialog(
                        context: context,
                        builder: (context) {
                          return CupertinoAlertDialog(
                            title: Text(
                                "Are you sure you want to close the test?"),
                            content: Text("Your results will not be saved"),
                            actions: [
                              CupertinoDialogAction(
                                  child: Text("Close the test"),
                                  onPressed: () {
                                    final answerController =
                                        Get.put(AnswerController());
                                    // answerController.getPracticeWidget();
                                    Get.back();
                                    Get.back();
                                  }),
                              CupertinoDialogAction(
                                  child: Text("Continue the test"),
                                  onPressed: () {
                                    Get.back();
                                  })
                            ],
                          );
                        });
                  },
                  child: Container(
                    child: Image.asset(
                      "assets/images/Close.png",
                      height: 24,
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    widget.test.name,
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        color: AppColors.black),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      if (widget.test.questions[widget.test.selectQuestion]
                              .selectAnswerId ==
                          -1) {
                        widget.test.questions[widget.test.selectQuestion]
                            .colorContainer = Color.fromRGBO(225, 225, 225, 1);
                      }
                      bool found = false;
                      {
                        for (int i = widget.test.selectQuestion + 1;
                            i < widget.test.questions.length;
                            i++) {
                          if (widget.test.questions[i].editExam) {
                            setState(() {
                              widget.test.selectQuestion = i;
                              found = true;
                            });
                            break;
                          }
                        }
                        if (!found) {
                          bool lastFound = false;

                          for (int i = 0;
                              i < widget.test.questions.length;
                              i++) {
                            if (widget.test.questions[i].editExam) {
                              lastFound = true;

                              break;
                            }
                          }
                          if (lastFound) {
                            showCupertinoDialog(
                                context: context,
                                builder: (context) {
                                  return CupertinoAlertDialog(
                                    title: Text("You skipped some questions"),
                                    content: Text(
                                        "Skipped questions count as mistakes. Would you like to go back and answer them?"),
                                    actions: [
                                      CupertinoDialogAction(
                                          child: Text("Finish the test"),
                                          onPressed: () {
                                            Get.to(() => PracticeResultPage(
                                                widget.test));
                                          }),
                                      CupertinoDialogAction(
                                          child: Text("Continue the test"),
                                          onPressed: () {
                                            // addPozition();
                                            Get.back();
                                          })
                                    ],
                                  );
                                });
                          }
                        }
                        if (!found) {
                          for (int i = 0;
                              i < widget.test.questions.length;
                              i++) {
                            if (widget.test.questions[i].editExam) {
                              setState(() {
                                widget.test.selectQuestion = i;
                                found = true;
                              });
                              break;
                            }
                          }
                        }
                        if (!found) {
                          Get.to(() => PracticeResultPage(widget.test));
                          print("END Test");
                        }
                      }
                    });

                    double _position = widget.test.selectQuestion * 63;
                    if (_position > scrollController.position.maxScrollExtent)
                      _position = scrollController.position.maxScrollExtent;
                    scrollController.animateTo(_position,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.linear);
                  },
                  child: Container(
                    child: Text(
                      "Skip",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: AppColors.purple),
                    ),
                  ),
                )),
            SizedBox(
              height: 12,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: DefaultTabController(
                  length: widget.test.questions.length,
                  child: Column(
                    children: [
                      Container(
                        height: 44,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            controller: scrollController,
                            itemCount: widget.test.questions.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                hoverColor: Colors.white.withOpacity(0),
                                splashColor: Colors.white.withOpacity(0),
                                focusColor: Colors.white.withOpacity(0),
                                highlightColor: Colors.white.withOpacity(0),
                                onTap: () {
                                  if (widget
                                          .test
                                          .questions[widget.test.selectQuestion]
                                          .selectAnswerId !=
                                      -1) {
                                    widget
                                        .test
                                        .questions[widget.test.selectQuestion]
                                        .editExam = false;
                                  }

                                  setState(() {
                                    for (int i = 0; i <= index; i++) {
                                      if (widget.test.questions[i]
                                              .selectAnswerId ==
                                          -1) {
                                        widget.test.questions[i]
                                                .colorContainer =
                                            Color.fromRGBO(225, 225, 225, 1);
                                      }
                                    }
                                    widget.test.selectQuestion = index;

                                    // if (widget.skipExam.indexOf(index) != -1) {
                                    //   widget.selectSkip =
                                    //       widget.skipExam.indexOf(index);
                                    // }
                                  });
                                },
                                child: tabBarWidget(
                                    index,
                                    index == widget.test.selectQuestion,
                                    widget.test.questions[index],
                                    index > widget.test.selectQuestion),
                              );
                            }),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                            "Question ${widget.test.selectQuestion + 1} of ${widget.test.questions.length}"
                                .toUpperCase(),
                            textAlign: TextAlign.left,
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: AppColors.gray)),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          widget
                              .test.questions[widget.test.selectQuestion].title,
                          textAlign: TextAlign.start,
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: AppColors.black),
                        ),
                      ),
                      if (widget.test.questions[widget.test.selectQuestion]
                          .image.isNotEmpty)
                        Container(
                            margin: EdgeInsets.only(top: 12),
                            height: 152,
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(vertical: 16),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12)),
                            child: SvgPicture.asset(
                              "assets/svg/" +
                                  widget
                                      .test
                                      .questions[widget.test.selectQuestion]
                                      .image,
                            )),
                      SizedBox(
                        height: 24,
                      ),
                      Expanded(
                          child: ListView.builder(
                              itemCount: widget
                                  .test
                                  .questions[widget.test.selectQuestion]
                                  .answers
                                  .length,
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    if (widget
                                        .test
                                        .questions[widget.test.selectQuestion]
                                        .editExam) {
                                      setState(() {
                                        widget
                                            .test
                                            .questions[
                                                widget.test.selectQuestion]
                                            .selectAnswerId = index;
                                        if (widget
                                                .test
                                                .questions[
                                                    widget.test.selectQuestion]
                                                .selectAnswerId ==
                                            widget
                                                .test
                                                .questions[
                                                    widget.test.selectQuestion]
                                                .correntAnswerId) {
                                          widget
                                                  .test
                                                  .questions[widget
                                                      .test.selectQuestion]
                                                  .colorContainer =
                                              AppColors.success
                                                  .withOpacity(0.2);
                                          widget
                                              .test
                                              .questions[
                                                  widget.test.selectQuestion]
                                              .colorText = AppColors.success;
                                          widget.test.questions[
                                              widget.test.selectQuestion];
                                        } else {
                                          widget
                                                  .test
                                                  .questions[widget
                                                      .test.selectQuestion]
                                                  .colorContainer =
                                              AppColors.red.withOpacity(0.2);
                                          widget
                                              .test
                                              .questions[
                                                  widget.test.selectQuestion]
                                              .colorText = AppColors.red;
                                        }
                                      });
                                    }
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(18),
                                    margin: EdgeInsets.only(bottom: 12),
                                    // height: 56,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(16)),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          "assets/images/${getCheckbox(index)}.png",
                                          width: 24,
                                        ),
                                        SizedBox(
                                          width: 18,
                                        ),
                                        Expanded(
                                          child: Text(
                                            widget
                                                .test
                                                .questions[
                                                    widget.test.selectQuestion]
                                                .answers[index]
                                                .text,
                                            style: GoogleFonts.inter(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 16,
                                                color: AppColors.black),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              })),
                    ],
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                if (widget
                    .test.questions[widget.test.selectQuestion].editExam) {
                  if (widget.test.questions[widget.test.selectQuestion]
                          .selectAnswerId !=
                      -1) {
                    setState(() {
                      widget.test.questions[widget.test.selectQuestion]
                          .editExam = false;
                      if (widget.test.questions[widget.test.selectQuestion]
                              .selectAnswerId ==
                          widget.test.questions[widget.test.selectQuestion]
                              .correntAnswerId) {
                        widget.test.questions[widget.test.selectQuestion]
                                .colorContainer =
                            AppColors.success.withOpacity(0.2);
                        widget.test.questions[widget.test.selectQuestion]
                            .colorText = AppColors.success;
                        widget.test.questions[widget.test.selectQuestion];
                      } else {
                        widget.test.questions[widget.test.selectQuestion]
                            .colorContainer = AppColors.red.withOpacity(0.2);
                        widget.test.questions[widget.test.selectQuestion]
                            .colorText = AppColors.red;
                      }
                    });
                  }
                } else {
                  if (widget.test.questions[widget.test.selectQuestion]
                          .selectAnswerId !=
                      -1) {
                    widget.test.questions[widget.test.selectQuestion].editExam =
                        false;
                  }

                  setState(() {
                    if (widget.test.questions[widget.test.selectQuestion]
                            .selectAnswerId ==
                        -1) {
                      widget.test.questions[widget.test.selectQuestion]
                          .colorContainer = Color.fromRGBO(225, 225, 225, 1);
                    }
                    {
                      bool found = false;
                      {
                        for (int i = widget.test.selectQuestion + 1;
                            i < widget.test.questions.length;
                            i++) {
                          if (widget.test.questions[i].editExam) {
                            setState(() {
                              widget.test.selectQuestion = i;
                              found = true;
                            });
                            break;
                          }
                        }
                        if (!found) {
                          bool lastFound = false;

                          for (int i = 0;
                              i < widget.test.questions.length;
                              i++) {
                            if (widget.test.questions[i].editExam) {
                              lastFound = true;

                              break;
                            }
                          }
                          if (lastFound) {
                            showCupertinoDialog(
                                context: context,
                                builder: (context) {
                                  return CupertinoAlertDialog(
                                    title: Text("You skipped some questions"),
                                    content: Text(
                                        "Skipped questions count as mistakes. Would you like to go back and answer them?"),
                                    actions: [
                                      CupertinoDialogAction(
                                          child: Text("Finish the test"),
                                          onPressed: () {
                                            Get.to(() => PracticeResultPage(
                                                widget.test));
                                          }),
                                      CupertinoDialogAction(
                                          child: Text("Continue the test"),
                                          onPressed: () {
                                            // addPozition();
                                            Get.back();
                                          })
                                    ],
                                  );
                                });
                          }
                        }
                      }

                      if (!found) {
                        for (int i = 0; i < widget.test.questions.length; i++) {
                          if (widget.test.questions[i].editExam) {
                            setState(() {
                              widget.test.selectQuestion = i;
                              found = true;
                            });
                            break;
                          }
                        }
                      }
                      if (!found) {
                        Get.to(() => PracticeResultPage(widget.test));
                        print("END Test");
                      }
                    }

                    double _position = widget.test.selectQuestion * 63;
                    if (_position > scrollController.position.maxScrollExtent)
                      _position = scrollController.position.maxScrollExtent;
                    scrollController.animateTo(_position,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.linear);
                  });
                }
              },
              child: Opacity(
                opacity: widget.test.questions[widget.test.selectQuestion]
                            .selectAnswerId ==
                        -1
                    ? 0.4
                    : 1,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  width: double.infinity,
                  height: 56,
                  decoration: BoxDecoration(
                      color: AppColors.purple,
                      borderRadius: BorderRadius.circular(16)),
                  child: Center(
                    child: Text(
                      widget.test.questions[widget.test.selectQuestion].editExam
                          ? "Check"
                          : "Next",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget tabBarWidget(int index, bool select, Question test, bool ahead) {
    return Container(
        margin: EdgeInsets.only(
            right: index + 1 != widget.test.questions.length ? 20 : 0),
        width: 44,
        height: 44,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: getBackColor(test, select)),
        child: Center(
          child: Text(
            (index + 1).toString(),
            style: GoogleFonts.inter(
              fontWeight: FontWeight.w500,
              fontSize: 20,
              color: select ? Colors.white : test.colorText,
            ),
          ),
        ));
  }

  Color getBackColor(Question test, bool select) {
    if (select) {
      if (!test.editExam) {
        if (test.colorContainer == AppColors.red.withOpacity(0.2)) {
          return AppColors.red;
        }
        if (test.colorContainer == AppColors.success.withOpacity(0.2)) {
          return AppColors.success;
        }
      }
      return AppColors.purple;
    } else {
      return test.colorContainer;
    }
  }

  String getCheckbox(int index) {
    if (widget.test.questions[widget.test.selectQuestion].editExam) {
      if (widget.test.questions[widget.test.selectQuestion].selectAnswerId ==
          index) {
        return "CheckboxNetural";
      } else {
        return "CheckboxNotSelect";
      }
    } else {
      if (widget.test.questions[widget.test.selectQuestion].selectAnswerId ==
              widget
                  .test.questions[widget.test.selectQuestion].correntAnswerId &&
          widget.test.questions[widget.test.selectQuestion].selectAnswerId ==
              index) {
        return "CheckboxTrue";
      }
      if (widget.test.questions[widget.test.selectQuestion].selectAnswerId !=
              widget
                  .test.questions[widget.test.selectQuestion].correntAnswerId &&
          widget.test.questions[widget.test.selectQuestion].selectAnswerId ==
              index) {
        return "CheckboxFalse";
      }
      if (index ==
          widget.test.questions[widget.test.selectQuestion].correntAnswerId) {
        return "CheckboxTrue";
      }
      return "CheckboxNotSelect";
    }
  }
}
