import 'dart:math';

import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/controllers/history_controller.dart';
import 'package:dmv_test/controllers/settings_controller.dart';
import 'package:dmv_test/models/history.dart';
import 'package:dmv_test/pages/main_page.dart';
import 'package:dmv_test/pages/questions_exam_page.dart';
import 'package:dmv_test/pages/questions_practice_page.dart';
import 'package:dmv_test/shared/appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

import '../models/answer.dart';

class PracticeResultPage extends StatefulWidget {
  int trueQuestion = 0;
  int notQuestion = 0;
  int falseQuestion = 0;
  bool animation = true;
  bool examPassed = false;
  Test test;
  bool fromHistory;
  int index;
  PracticeResultPage(this.test,
      {this.fromHistory = false, this.index = 0, Key? key})
      : super(key: key);

  @override
  State<PracticeResultPage> createState() => _PracticeResultPageState();
}

class _PracticeResultPageState extends State<PracticeResultPage>
    with TickerProviderStateMixin {
  late final AnimationController _controller;
  final historyController = Get.put(HistoryController());
  final answerController = Get.put(AnswerController());
  final settingsController = Get.put(SettingsController());
  @override
  void initState() {
    // TODO: implement initState
    _controller = AnimationController(vsync: this);

    widget.test.testEnd = true;

    double valuePassed = settingsController.state.correctAnswers /
        settingsController.state.question *
        widget.test.questions.length;

    if (answerController.getTrueAnswer(widget.test) >= valuePassed) {
      widget.examPassed = true;
    } else {
      widget.examPassed = false;
    }

    var allTestEdit = answerController.allTest
        .firstWhere((element) => element.name == widget.test.name);

    allTestEdit.bestResult = max(
        answerController.getTrueAnswer(widget.test), widget.test.bestResult);

    for (int i = 0; i < widget.test.questions.length; i++) {
      if (widget.test.questions[i].selectAnswerId ==
          widget.test.questions[i].correntAnswerId) {
        widget.trueQuestion++;
      } else {
        if (widget.test.questions[i].selectAnswerId == -1)
          widget.notQuestion++;
        else
          widget.falseQuestion++;
      }
    }
    print("Add");
    if (!widget.fromHistory) {
      print(historyController.historys.length);
      historyController.addElement(History(
          falseAnswer: widget.falseQuestion,
          nameTest: widget.test.name,
          passed: widget.examPassed,
          allAnswer: widget.test.questions.length,
          correctAnswer: widget.trueQuestion,
          time: DateFormat("d.MM.yy, hh:mm").format(DateTime.now()),
          test: Test.copyWith(widget.test)));

      print(historyController.historys.length);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => Scaffold(
              backgroundColor: AppColors.backgroundColor,
              body: SafeArea(
                  top: false,
                  child: Column(
                    children: [
                      widget.fromHistory
                          ? CustomAppBar(
                              InkWell(
                                onTap: () {
                                  Get.back();
                                },
                                child: Container(
                                  child: Image.asset(
                                    "assets/images/Close.png",
                                    width: 24,
                                  ),
                                ),
                              ),
                              Text(
                                "Result",
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                    color: AppColors.black),
                              ),
                              InkWell(
                                onTap: () {
                                  showCupertinoDialog(
                                      context: context,
                                      builder: (context) {
                                        return CupertinoAlertDialog(
                                          title: Text("Delete tests results?"),
                                          content: Text(
                                              "This action cannot be undone"),
                                          actions: [
                                            CupertinoDialogAction(
                                                child: Text("Cancel"),
                                                onPressed: () {
                                                  Get.back();
                                                }),
                                            CupertinoDialogAction(
                                                child: Text(
                                                  "Delete",
                                                  style: TextStyle(
                                                      color: Colors.red),
                                                ),
                                                onPressed: () {
                                                  historyController
                                                      .deleteOneHistory(
                                                          widget.index);
                                                  Get.back();
                                                  Get.back();
                                                })
                                          ],
                                        );
                                      });
                                },
                                child: Container(
                                  child: Image.asset(
                                    "assets/images/delete.png",
                                    width: 24,
                                  ),
                                ),
                              ))
                          : CustomAppBar(
                              Container(),
                              Text(
                                "Result",
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                    color: AppColors.black),
                              ),
                              Container()),
                      SizedBox(
                        height: 24,
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 24),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(16)),
                                  child: Column(
                                    children: [
                                      Text(
                                        widget.examPassed
                                            ? "Test passed"
                                            : "Test failed",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 24,
                                            color: AppColors.black),
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          SizedBox(
                                            width: 240.h,
                                            height: 240.h,
                                            child: circularWidget(
                                                double.parse(widget.trueQuestion
                                                    .toString()),
                                                double.parse(widget
                                                    .test.questions.length
                                                    .toString()),
                                                double.parse(widget.notQuestion
                                                    .toString())),
                                          ),
                                          Center(
                                            child: Text(
                                              (widget.trueQuestion /
                                                          (widget.test.questions
                                                                  .length /
                                                              100))
                                                      .toInt()
                                                      .toString() +
                                                  "%",
                                              style: GoogleFonts.inter(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 36,
                                                  color: AppColors.black),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Row(
                                            children: [
                                              Image.asset(
                                                "assets/images/ResultTrue.png",
                                                width: 20,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                widget.trueQuestion.toString(),
                                                style: GoogleFonts.inter(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                    color: AppColors.black),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            width: 40,
                                          ),
                                          Row(
                                            children: [
                                              Image.asset(
                                                "assets/images/NotResult.png",
                                                width: 20,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                widget.notQuestion.toString(),
                                                style: GoogleFonts.inter(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                    color: AppColors.black),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            width: 40,
                                          ),
                                          Row(
                                            children: [
                                              Image.asset(
                                                "assets/images/ResultFalse.png",
                                                width: 20,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                widget.falseQuestion.toString(),
                                                style: GoogleFonts.inter(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16,
                                                    color: AppColors.black),
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 48,
                                ),
                                InkWell(
                                  onTap: () {
                                    Get.to(() =>
                                        QuestionsPracticePage(widget.test));
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    height: 56,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: AppColors.purple),
                                    child: Center(
                                      child: Text(
                                        "Review answers",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 16,
                                ),
                                InkWell(
                                  onTap: () {
                                    _controller.dispose();
                                    if (widget.fromHistory) {
                                      Get.back();
                                    } else {
                                      historyController.update();
                                      Get.offAll(() => MainPage());
                                    }
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 20),
                                    width: double.infinity,
                                    height: 56,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.white,
                                        border: Border.all(
                                            color: AppColors.purple)),
                                    child: Center(
                                      child: Text(
                                        "Exit",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            color: AppColors.purple),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ));
  }

  stopAnim(Duration seconds) async {
    await Future.delayed(seconds);
    setState(() {
      widget.animation = false;
    });
  }

  Widget circularWidget(double trueAnswer, double allAnswer, double notAnswer) {
    return SfRadialGauge(
      axes: <RadialAxis>[
        RadialAxis(
            startAngle: 270,
            endAngle: 630,
            showTicks: false,
            labelFormat: '',
            // onLabelCreated: _handleLabelCreated,
            showAxisLine: false,
            radiusFactor: 0.9,
            minimum: 0,
            maximum: allAnswer,
            labelOffset: 25,
            interval: 1,
            canRotateLabels: true,
            pointers: <GaugePointer>[
              if (trueAnswer != 0)
                WidgetPointer(
                  value: trueAnswer,
                  offset: 11,
                  child: Image.asset(
                    "assets/images/ResultTrue.png",
                    width: 28,
                  ),
                ),
              if (notAnswer != 0)
                WidgetPointer(
                  value: notAnswer + trueAnswer,
                  offset: 11,
                  child: Image.asset(
                    "assets/images/NotResult.png",
                    width: 28,
                  ),
                ),
              if (notAnswer + trueAnswer != allAnswer)
                WidgetPointer(
                  value: 0,
                  offset: 11,
                  child: Image.asset(
                    "assets/images/ResultFalse.png",
                    width: 28,
                  ),
                ),
            ],
            ranges: <GaugeRange>[
              GaugeRange(
                  startValue: 0,
                  endValue: trueAnswer,
                  color: Color.fromRGBO(218, 243, 225, 1),
                  sizeUnit: GaugeSizeUnit.factor,
                  rangeOffset: 0.06,
                  startWidth: 0.2,
                  endWidth: 0.2),
              GaugeRange(
                  startValue: trueAnswer,
                  endValue: notAnswer + trueAnswer,
                  rangeOffset: 0.06,
                  sizeUnit: GaugeSizeUnit.factor,
                  color: AppColors.gray.withOpacity(0.1),
                  startWidth: 0.2,
                  endWidth: 0.2),
              GaugeRange(
                  startValue: notAnswer + trueAnswer,
                  endValue: allAnswer,
                  rangeOffset: 0.06,
                  sizeUnit: GaugeSizeUnit.factor,
                  color: Color.fromRGBO(251, 222, 219, 1),
                  startWidth: 0.2,
                  endWidth: 0.2),
            ]),
      ],
    );
  }
}
