import 'package:apphud/apphud.dart';
import 'package:apphud/models/apphud_models/apphud_paywall.dart';
import 'package:apphud/models/apphud_models/apphud_product.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/pages/onboarding/onboarding_page3.dart';
import 'package:dmv_test/services/browser_services.dart';
import 'package:dmv_test/shared/background_onboard.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../main.dart';

class SubscriptionPage extends StatefulWidget {
  int tap = 1;
  @override
  State<SubscriptionPage> createState() => _SubscriptionPageState();
}

class _SubscriptionPageState extends State<SubscriptionPage> {
  Future<bool> pressBuySubscription(int type) async {
    final subID = subs[type];
    final paywalls = await Apphud.paywallsDidLoadCallback();
    for (ApphudPaywall paywall in paywalls.paywalls) {
      for (ApphudProduct product in paywall.products!) {
        if (product.productId == subID) {
          final res = await Apphud.purchase(product: product);
          if (kDebugMode || res.subscription!.isActive) {
            return true;
          }
        }
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => Scaffold(
              body: Stack(children: [
                backgroundOnboard(),
                SafeArea(
                  bottom: false,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 24, vertical: 20),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: InkWell(
                                onTap: () async {
                                  final answerController =
                                      Get.put(AnswerController());
                                  final result =
                                      await Apphud.restorePurchases();
                                  if (result.subscriptions
                                      .map((e) => e.isActive)
                                      .contains(true)) {
                                    answerController.subscribe.value = true;

                                    Get.back();
                                  }
                                },
                                child: Text(
                                  "Restore",
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      color: AppColors.gray),
                                ),
                              ),
                            ),
                          ),
                          Container(
                              // color: Colors.purple,
                              child: Center(
                            child: Image.asset(
                              "assets/images/subscriptionImage.png",
                              fit: BoxFit.fitWidth,
                              height: 375.h,
                            ),
                          )),
                        ],
                      ),
                      Text(
                        "Prepare for\nDMV Practice Exam",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.inter(
                            color: AppColors.black,
                            fontSize: 32.sp,
                            fontWeight: FontWeight.w800),
                      ),
                      SizedBox(
                        height: 24.h,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 32),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                setState(() {
                                  widget.tap = 0;
                                });
                              },
                              child: AnimatedContainer(
                                padding: EdgeInsets.all(16),
                                width: 140.w,
                                height: 120.w,
                                duration: Duration(milliseconds: 300),
                                decoration: BoxDecoration(
                                    color: widget.tap == 0
                                        ? Color.fromRGBO(219, 212, 241, 1)
                                        : Color.fromRGBO(240, 240, 240, 1),
                                    borderRadius: BorderRadius.circular(16)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Weekly",
                                          style: GoogleFonts.inter(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              color: AppColors.gray),
                                        ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text(
                                          "\$7.99",
                                          style: GoogleFonts.inter(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 19.sp,
                                              color: AppColors.black),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      width: 20.w,
                                      height: 20.w,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          color: widget.tap == 0
                                              ? AppColors.purple
                                              : Colors.white),
                                      child: Center(
                                        child: Image.asset(
                                          "assets/images/onboarding3_3.png",
                                          width: 15,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  widget.tap = 1;
                                });
                              },
                              child: AnimatedContainer(
                                padding: EdgeInsets.all(16),
                                width: 140.w,
                                height: 120.w,
                                duration: Duration(milliseconds: 300),
                                decoration: BoxDecoration(
                                    color: widget.tap == 1
                                        ? Color.fromRGBO(219, 212, 241, 1)
                                        : Color.fromRGBO(240, 240, 240, 1),
                                    borderRadius: BorderRadius.circular(16)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Monthly",
                                          style: GoogleFonts.inter(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12.sp,
                                              color: AppColors.gray),
                                        ),
                                        SizedBox(
                                          height: 8.h,
                                        ),
                                        Expanded(
                                          child: AutoSizeText(
                                            "\$19.99",
                                            style: GoogleFonts.inter(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 19.sp,
                                                color: AppColors.black),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 12.h,
                                        ),
                                        Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 12.h,
                                              vertical: 4.5.h),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              color: Colors.white),
                                          child: Text(
                                            "Save 40 %",
                                            style: GoogleFonts.inter(
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w700,
                                                color: Color.fromRGBO(
                                                    244, 185, 107, 1)),
                                          ),
                                        )
                                      ],
                                    ),
                                    Container(
                                      width: 20.h,
                                      height: 20.h,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          color: widget.tap == 1
                                              ? AppColors.purple
                                              : Colors.white),
                                      child: Center(
                                        child: Image.asset(
                                          "assets/images/onboarding3_3.png",
                                          width: 15.h,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: () async {
                                  final answerController =
                                      Get.put(AnswerController());
                                  if (kDebugMode) {
                                    Get.back();
                                    answerController.subscribe.value = true;
                                    answerController.update();
                                  } else {
                                    if (await pressBuySubscription(
                                        widget.tap)) {
                                      answerController.subscribe.value = true;
                                      Get.back();
                                      answerController.update();
                                    } else {
                                      answerController.subscribe.value = false;
                                      Get.back();
                                      answerController.update();
                                    }
                                  }

                                  // answerController.getPracticeWidget();
                                },
                                child: Image.asset(
                                  "assets/images/subscribe_button.png",
                                  width: 76.h,
                                ),
                              ),
                              Container(
                                height: 40,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    InkWell(
                                      onTap: termsOfUsePressed,
                                      child: Text(
                                        "Term of Use",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14.sp,
                                            color: AppColors.gray),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Get.back();
                                      },
                                      child: Text(
                                        "Close",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14.sp,
                                            color: AppColors.gray),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: privacyPolicyPressed,
                                      child: Text(
                                        "Privacy Policy",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14.sp,
                                            color: AppColors.gray),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 29.h,
                      )
                    ],
                  ),
                )
              ]),
            ));
  }
}
