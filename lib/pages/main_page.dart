import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/history_controller.dart';
import 'package:dmv_test/pages/home_page.dart';
import 'package:dmv_test/pages/settings_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/navigation_bar_controller.dart';
import '../services/save_and_download.dart';
import 'history_page.dart';

class MainPage extends StatefulWidget {
  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with WidgetsBindingObserver {
  final bottomBarController = Get.put(NavigationBarController());
  final historyController = Get.put(HistoryController());
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print(state);
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.inactive) {
      saveAllInfo();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HistoryController>(builder: (_) {
      return GetBuilder<NavigationBarController>(builder: (_) {
        return ScreenUtilInit(
            designSize: const Size(375, 844),
            builder: (_) => Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: AppColors.backgroundColor,
                bottomNavigationBar: Obx(
                  () => Container(
                    height:
                        historyController.selectedDelete.value ? 86.h : 116.h,
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 8),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    spreadRadius: 6,
                                    blurRadius: 10,
                                    offset: Offset(0, -6),
                                    color: Color.fromRGBO(0, 0, 0, 0.02))
                              ],
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(28),
                                  topRight: Radius.circular(28))),
                          child: BottomNavigationBar(
                            onTap: bottomBarController.changeTabIndex,
                            currentIndex: bottomBarController.tabIndex.value,
                            showSelectedLabels: false,
                            showUnselectedLabels: false,
                            type: BottomNavigationBarType.fixed,
                            backgroundColor: Colors.white.withOpacity(0),
                            elevation: 0,
                            items: [
                              bottomNavigationBarItem(
                                  "History", "History", context),
                              bottomNavigationBarItem("Home", "Home", context),
                              bottomNavigationBarItem(
                                  "Settings", "Settings", context),
                            ],
                          ),
                        ),
                        Obx(
                          () => historyController.selectedDelete.value
                              ? InkWell(
                                  onTap: () {
                                    showCupertinoDialog(
                                        context: context,
                                        builder: (context) {
                                          return CupertinoAlertDialog(
                                            title:
                                                Text("Delete tests results?"),
                                            content: Text(
                                                "This action cannot be undone"),
                                            actions: [
                                              CupertinoDialogAction(
                                                  child: Text("Cancel"),
                                                  onPressed: () {
                                                    Get.back();
                                                  }),
                                              CupertinoDialogAction(
                                                  child: Text(
                                                    "Delete",
                                                    style: TextStyle(
                                                        color: Colors.red),
                                                  ),
                                                  onPressed: () {
                                                    historyController
                                                        .deleteHistory();
                                                    Get.back();
                                                  })
                                            ],
                                          );
                                        });
                                  },
                                  child: Container(
                                    height: 111,
                                    padding: EdgeInsets.only(top: 16),
                                    alignment: Alignment.topCenter,
                                    color: Colors.white,
                                    child: Image.asset(
                                      "assets/images/DeleteEnable.png",
                                      width: 24,
                                    ),
                                  ),
                                )
                              : Container(),
                        )
                      ],
                    ),
                  ),
                ),
                body: ScreenUtilInit(
                  designSize: const Size(375, 844),
                  builder: (_) => IndexedStack(
                    index: bottomBarController.pageIndex,
                    children: [
                      HistoryPage(),
                      HomePage(),
                      SettingsPage(),
                    ],
                  ),
                )));
      });
    });
  }

  BottomNavigationBarItem bottomNavigationBarItem(
      String image, String label, BuildContext context) {
    return BottomNavigationBarItem(
        icon: Column(
          children: [
            Image.asset(
              "assets/images/${image}.png",
              width: 24,
            ),
            Container(
              height: 10,
            )
          ],
        ),
        activeIcon: Column(
          children: [
            Image.asset("assets/images/${image}_enabled.png", width: 24),
            Text(
              label,
              style: GoogleFonts.manrope(
                  fontWeight: FontWeight.w500,
                  fontSize: 10,
                  color: AppColors.purple),
            )
          ],
        ),
        label: label);
  }
}
