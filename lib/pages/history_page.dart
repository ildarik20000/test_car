import 'package:auto_size_text/auto_size_text.dart';
import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/history_controller.dart';
import 'package:dmv_test/shared/appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

import '../models/answer.dart';
import 'exam_result_page.dart';
import 'practice_result_page.dart';

class HistoryPage extends StatefulWidget {
  HistoryPage({Key? key}) : super(key: key);

  @override
  State<HistoryPage> createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage>
    with TickerProviderStateMixin {
  final historyController = Get.put(HistoryController());

  late final AnimationController _controller;
  @override
  void initState() {
    // TODO: implement initState

    _controller = AnimationController(vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HistoryController>(builder: (_) {
      // historyController.getHistoryWidget();
      return SafeArea(
        top: false,
        child: Column(
          children: [
            CustomAppBar(
              Container(
                child: historyController.selectedDelete.value
                    ? InkWell(
                        onTap: () {
                          historyController.save();
                        },
                        child: Text(
                          "Save",
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: AppColors.black),
                        ),
                      )
                    : Text(
                        "History",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w600,
                            fontSize: 24,
                            color: AppColors.black),
                      ),
              ),
              historyController.selectedDelete.value
                  ? Container(
                      child: Text(
                        "${historyController.selectListDelete.length.toString()}/${historyController.historys.length}",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w500,
                            fontSize: 20,
                            color: AppColors.black),
                      ),
                    )
                  : Container(),
              historyController.historys.isNotEmpty
                  ? historyController.selectedDelete.value
                      ? InkWell(
                          onTap: () {
                            historyController.selectedAll();
                          },
                          child: Text("Select all",
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16,
                                  color: AppColors.black)),
                        )
                      : InkWell(
                          onTap: () {
                            historyController.selectedDelete.value = true;
                            historyController.update();
                          },
                          child: Container(
                            child: Image.asset(
                              "assets/images/delete.png",
                              width: 24,
                            ),
                          ),
                        )
                  : Container(),
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Container(
                        alignment: Alignment.topLeft,
                        child: Wrap(
                          spacing: 16,
                          runSpacing: 16,
                          children: [
                            for (int index = 0;
                                index < historyController.historys.length;
                                index++)
                              elementHistory(
                                  test: historyController.historys[index].test,
                                  exam: historyController
                                          .historys[index].nameTest ==
                                      "Exam",
                                  passed:
                                      historyController.historys[index].passed,
                                  index: index,
                                  allAnswer: historyController
                                      .historys[index].allAnswer,
                                  correctAnswer: historyController
                                      .historys[index].correctAnswer,
                                  nameTest: historyController
                                      .historys[index].nameTest,
                                  time: historyController.historys[index].time),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Opacity(
                    opacity: historyController.historys.isNotEmpty ? 0 : 1,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 92,
                            child: Lottie.asset(
                              'assets/animation/history_no_data.json',
                              controller: _controller,
                              onLoaded: (composition) {
                                _controller
                                  ..duration = composition.duration
                                  ..repeat();
                              },
                            ),
                          ),
                          Text(
                            "No data found",
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: AppColors.gray),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
    });
  }

  Widget elementHistory(
      {required Test test,
      required bool exam,
      required int index,
      required int allAnswer,
      required int correctAnswer,
      required String nameTest,
      required bool passed,
      required String time}) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => InkWell(
              onTap: () {
                if (historyController.selectedDelete.value) {
                  if (historyController.selectListDelete.contains(index)) {
                    historyController.selectListDelete.remove(index);
                  } else {
                    historyController.selectListDelete.add(index);
                  }
                  // getHistoryWidget();
                } else {
                  if (exam)
                    Get.to(() => ExamResultPage(
                          test,
                          fromHistory: true,
                          index: index,
                        ));
                  else
                    Get.to(() => PracticeResultPage(
                          test,
                          fromHistory: true,
                          index: index,
                        ));
                }
                setState(() {});
              },
              child: Stack(
                children: [
                  Container(
                    width: 159.w,
                    height: 154,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: Colors.white),
                    child: Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 36,
                                width: 36,
                                child: SleekCircularSlider(
                                    appearance: CircularSliderAppearance(
                                        infoProperties: InfoProperties(
                                            mainLabelStyle: TextStyle(
                                                color: Color.fromRGBO(
                                                    0, 0, 0, 0))),
                                        startAngle: -90,
                                        angleRange: 360,
                                        customColors: CustomSliderColors(
                                            dotColor:
                                                Color.fromRGBO(0, 0, 0, 0),
                                            // dynamicGradient: true,
                                            trackGradientStartAngle: 0,
                                            trackGradientEndAngle: 0,
                                            progressBarColor: passed
                                                ? Colors.green
                                                : AppColors.red,
                                            // progressBarColors: [
                                            //   AppColors.purple,
                                            //   Color.fromRGBO(223, 218, 244, 1),
                                            // ],
                                            trackColor: Color.fromRGBO(
                                                240, 240, 240, 1)),
                                        customWidths: CustomSliderWidths(
                                            shadowWidth: 0,
                                            trackWidth: 5,
                                            progressBarWidth:
                                                correctAnswer.toDouble() > 0
                                                    ? 5
                                                    : 0)),
                                    min: 0,
                                    max: allAnswer.toDouble(),
                                    initialValue: correctAnswer.toDouble()),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              Text(
                                "${correctAnswer} of ${allAnswer} correct",
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12.sp,
                                    color: AppColors.gray),
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Expanded(
                                child: AutoSizeText(
                                  nameTest,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.sp,
                                      color: AppColors.black),
                                ),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              passed
                                  ? Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 4),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          color: AppColors.success
                                              .withOpacity(0.2)),
                                      child: Text(
                                        "Passed",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12,
                                            color: AppColors.success),
                                      ),
                                    )
                                  : Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 4),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          color:
                                              AppColors.red.withOpacity(0.2)),
                                      child: Text(
                                        "Failed",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12,
                                            color: AppColors.red),
                                      ),
                                    )
                            ],
                          ),
                        ),
                        Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              width: 83,
                              height: 28,
                              decoration: BoxDecoration(
                                  color: AppColors.gray.withOpacity(0.06),
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(16),
                                    topRight: Radius.circular(16),
                                  )),
                              child: Center(
                                child: Text(
                                  time,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 10,
                                      color: AppColors.gray),
                                ),
                              ),
                            )),
                      ],
                    ),
                  ),
                  if (historyController.selectListDelete.contains(index))
                    Container(
                      width: 163,
                      height: 154,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.black.withOpacity(0.1)),
                    ),
                ],
              ),
            ));
  }
}
