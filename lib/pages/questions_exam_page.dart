import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/pages/questions_test_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';

import '../config/colors.dart';
import '../models/answer.dart';
import '../shared/appbar.dart';

class QuestionsExamPage extends StatefulWidget {
  QuestionsExamPage({Key? key}) : super(key: key);

  @override
  State<QuestionsExamPage> createState() => _QuestionsExamPageState();
}

class _QuestionsExamPageState extends State<QuestionsExamPage> {
  final answerController = Get.put(AnswerController());
  @override
  void initState() {
    answerController.allElements.clear();
    answerController.correctElements.clear();
    answerController.wrongElements.clear();
    answerController.allQuestion.clear();
    answerController.correctQuestion.clear();
    answerController.wrongQuestion.clear();
    answerController.allIndex.clear();
    answerController.correctIndex.clear();
    answerController.wrongIndex.clear();
    for (int i = 0; i < answerController.examTest.questions.length; i++) {
      if (answerController.examTest.questions[i].selectAnswerId ==
          answerController.examTest.questions[i].correntAnswerId) {
        answerController.allIndex.add(i);
        answerController.correctIndex.add(i);
        answerController.allQuestion
            .add(answerController.examTest.questions[i]);

        answerController.correctQuestion
            .add(answerController.examTest.questions[i]);
        answerController.allElements
            .add(elementContainer(true, i + 1, "All", i, 0));
        answerController.correctElements
            .add(elementContainer(true, i + 1, "Correct", i, 1));
      } else {
        answerController.allIndex.add(i);
        answerController.wrongIndex.add(i);
        answerController.allQuestion
            .add(answerController.examTest.questions[i]);
        answerController.wrongQuestion
            .add(answerController.examTest.questions[i]);
        answerController.allElements
            .add(elementContainer(false, i + 1, "All", i, 0));
        answerController.wrongElements
            .add(elementContainer(false, i + 1, "Wrong", i, 2));
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.backgroundColor,
        body: SafeArea(
            top: false,
            child: Column(children: [
              CustomAppBar(
                  InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      child: Image.asset(
                        "assets/images/Close.png",
                        width: 24,
                        height: 24,
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      "Questions",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: AppColors.black),
                    ),
                  ),
                  Container()),
              SizedBox(
                height: 24,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      Expanded(
                        child: DefaultTabController(
                          length: 3,
                          child: Column(
                            children: [
                              Container(
                                height: 44,
                                padding: EdgeInsets.all(4),
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: Colors.white),
                                child: TabBar(
                                  labelStyle: GoogleFonts.inter(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16),
                                  indicator: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: AppColors.purple),
                                  labelColor: Colors.white,
                                  unselectedLabelColor: AppColors.purple,
                                  tabs: [
                                    Tab(
                                      text: "All",
                                    ),
                                    Tab(
                                      text: "Correct",
                                    ),
                                    Tab(
                                      text: "Wrong",
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 24,
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: TabBarView(
                                        children: [
                                          ListView(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.all(16),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            16),
                                                    color: Colors.white),
                                                child: Wrap(
                                                  spacing: 17.75,
                                                  runSpacing: 17.75,
                                                  children: answerController
                                                      .allElements,
                                                ),
                                              ),
                                            ],
                                          ),
                                          answerController
                                                  .correctElements.isNotEmpty
                                              ? ListView(
                                                  children: [
                                                    Container(
                                                      padding:
                                                          EdgeInsets.all(16),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(16),
                                                          color: Colors.white),
                                                      child: Wrap(
                                                        spacing: 17.75,
                                                        runSpacing: 17.75,
                                                        children:
                                                            answerController
                                                                .correctElements,
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              : Container(),
                                          answerController
                                                  .wrongElements.isNotEmpty
                                              ? ListView(
                                                  children: [
                                                    Container(
                                                      padding:
                                                          EdgeInsets.all(16),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(16),
                                                          color: Colors.white),
                                                      child: Wrap(
                                                        spacing: 17.75,
                                                        runSpacing: 17.75,
                                                        children:
                                                            answerController
                                                                .wrongElements,
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              : Container(),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ])));
  }

  Widget elementContainer(
      bool result, int number, String name, int tapIndex, int type) {
    return InkWell(
      onTap: () {
        Get.to(() => QuestionsTestPage(
              type,
              name,
              selectQuestion: tapIndex,
            ));
      },
      child: Container(
        width: 48,
        height: 48,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: result
                ? Color.fromRGBO(218, 243, 225, 1)
                : Color.fromRGBO(251, 222, 219, 1)),
        child: Center(
          child: Text(
            number.toString(),
            style: GoogleFonts.inter(
                fontWeight: FontWeight.w500,
                fontSize: 20,
                color: result ? AppColors.success : AppColors.red),
          ),
        ),
      ),
    );
  }
}
