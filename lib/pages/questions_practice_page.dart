import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/pages/questions_test_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';

import '../config/colors.dart';
import '../models/answer.dart';
import '../shared/appbar.dart';

class QuestionsPracticePage extends StatefulWidget {
  Test test;
  QuestionsPracticePage(this.test, {Key? key}) : super(key: key);

  @override
  State<QuestionsPracticePage> createState() => _QuestionsPracticePageState();
}

class _QuestionsPracticePageState extends State<QuestionsPracticePage> {
  final answerController = Get.put(AnswerController());
  @override
  void initState() {
    answerController.allElements.clear();
    answerController.correctElements.clear();
    answerController.wrongElements.clear();
    answerController.allQuestion.clear();
    answerController.correctQuestion.clear();
    answerController.wrongQuestion.clear();
    answerController.allIndex.clear();
    answerController.correctIndex.clear();
    answerController.wrongIndex.clear();
    answerController.skippedElements.clear();
    answerController.skippedIndex.clear();
    answerController.skippedQuestion.clear();
    for (int i = 0; i < widget.test.questions.length; i++) {
      if (widget.test.questions[i].selectAnswerId ==
          widget.test.questions[i].correntAnswerId) {
        answerController.allIndex.add(i);
        answerController.correctIndex.add(i);
        answerController.allQuestion.add(widget.test.questions[i]);

        answerController.correctQuestion.add(widget.test.questions[i]);
        answerController.allElements
            .add(elementContainer(0, i + 1, "All", i, 0));
        answerController.correctElements
            .add(elementContainer(0, i + 1, "Correct", i, 1));
      } else {
        if (widget.test.questions[i].selectAnswerId == -1) {
          answerController.allIndex.add(i);
          answerController.skippedIndex.add(i);
          answerController.allQuestion.add(widget.test.questions[i]);
          answerController.skippedQuestion.add(widget.test.questions[i]);
          answerController.allElements
              .add(elementContainer(2, i + 1, "All", i, 0));
          answerController.skippedElements
              .add(elementContainer(2, i + 1, "Skipped", i, 3));
        } else {
          answerController.allIndex.add(i);
          answerController.wrongIndex.add(i);
          answerController.allQuestion.add(widget.test.questions[i]);
          answerController.wrongQuestion.add(widget.test.questions[i]);
          answerController.allElements
              .add(elementContainer(1, i + 1, "All", i, 0));
          answerController.wrongElements
              .add(elementContainer(1, i + 1, "Wrong", i, 2));
        }
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => Scaffold(
            backgroundColor: AppColors.backgroundColor,
            body: SafeArea(
                top: false,
                child: Column(children: [
                  CustomAppBar(
                      InkWell(
                        onTap: () {
                          Get.back();
                        },
                        child: Container(
                          child: Image.asset(
                            "assets/images/Close.png",
                            width: 24,
                            height: 24,
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          "Questions",
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              color: AppColors.black),
                        ),
                      ),
                      Container()),
                  SizedBox(
                    height: 24,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        children: [
                          Expanded(
                            child: DefaultTabController(
                              length: 4,
                              child: Column(
                                children: [
                                  Container(
                                    height: 44,
                                    padding: EdgeInsets.all(4),
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color: Colors.white),
                                    child: TabBar(
                                      // padding: EdgeInsets.symmetric(horizontal: 10),
                                      labelPadding: EdgeInsets.symmetric(
                                          horizontal: 10.h),
                                      labelStyle: GoogleFonts.inter(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16.sp),
                                      indicator: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          color: AppColors.purple),
                                      labelColor: Colors.white,
                                      unselectedLabelColor: AppColors.purple,
                                      tabs: [
                                        Tab(
                                          text: "All",
                                        ),
                                        Tab(
                                          text: "Correct",
                                        ),
                                        Tab(
                                          text: "Wrong",
                                        ),
                                        Tab(
                                          text: "Skipped",
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 24,
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: TabBarView(
                                            children: [
                                              ListView(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.all(16),
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(16),
                                                        color: Colors.white),
                                                    child: Wrap(
                                                      spacing: 17.75,
                                                      runSpacing: 17.75,
                                                      children: answerController
                                                          .allElements,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              answerController.correctElements
                                                      .isNotEmpty
                                                  ? ListView(
                                                      children: [
                                                        Container(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  16),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          16),
                                                              color:
                                                                  Colors.white),
                                                          child: Wrap(
                                                            spacing: 17.75,
                                                            runSpacing: 17.75,
                                                            children:
                                                                answerController
                                                                    .correctElements,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : Container(),
                                              answerController
                                                      .wrongElements.isNotEmpty
                                                  ? ListView(
                                                      children: [
                                                        Container(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  16),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          16),
                                                              color:
                                                                  Colors.white),
                                                          child: Wrap(
                                                            spacing: 17.75,
                                                            runSpacing: 17.75,
                                                            children:
                                                                answerController
                                                                    .wrongElements,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : Container(),
                                              answerController.skippedElements
                                                      .isNotEmpty
                                                  ? ListView(
                                                      children: [
                                                        Container(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  16),
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          16),
                                                              color:
                                                                  Colors.white),
                                                          child: Wrap(
                                                            spacing: 17.75,
                                                            runSpacing: 17.75,
                                                            children:
                                                                answerController
                                                                    .skippedElements,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : Container(),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ]))));
  }

  Widget elementContainer(
      int result, int number, String name, int tapIndex, int type) {
    return InkWell(
      onTap: () {
        Get.to(() => QuestionsTestPage(
              type,
              name,
              selectQuestion: tapIndex,
            ));
      },
      child: Container(
        width: 48,
        height: 48,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: result == 0
                ? Color.fromRGBO(218, 243, 225, 1)
                : result != 1
                    ? AppColors.gray.withOpacity(0.1)
                    : Color.fromRGBO(251, 222, 219, 1)),
        child: Center(
          child: Text(
            number.toString(),
            style: GoogleFonts.inter(
                fontWeight: FontWeight.w500,
                fontSize: 20,
                color: result == 0
                    ? AppColors.success
                    : result != 1
                        ? AppColors.gray
                        : AppColors.red),
          ),
        ),
      ),
    );
  }
}
