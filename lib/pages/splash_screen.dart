import 'package:dmv_test/pages/main_page.dart';
import 'package:dmv_test/pages/subscription_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

import '../controllers/answer_controller.dart';
import 'onboarding/onboarding_page1.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final answerController = Get.put(AnswerController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      await Future.delayed(Duration(milliseconds: 500));

      if (Hive.box('settings').get('showOnboarding', defaultValue: false)) {
        Get.offAll(() => MainPage());
        if (!answerController.subscribe.value) Get.to(() => SubscriptionPage());
      } else {
        Get.to(() => OnboardingPage1());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Image.asset(
          "assets/images/splash.png",
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}
