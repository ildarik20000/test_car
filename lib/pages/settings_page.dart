import 'dart:io';

import 'package:app_review/app_review.dart';
import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/controllers/settings_controller.dart';
import 'package:dmv_test/services/browser_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';

import '../config/colors.dart';
import '../services/boxes.dart';
import '../shared/appbar.dart';
import 'edit_state_page.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final settingsController = Get.put(SettingsController());
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => SafeArea(
              bottom: false,
              top: false,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomAppBar(
                      Container(
                        child: Text(
                          "Settings",
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w600,
                              fontSize: 24,
                              color: AppColors.black),
                        ),
                      ),
                      Container(),
                      Container()),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 31,
                            ),
                            Text(
                              "Select licencse type",
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: AppColors.gray),
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            DefaultTabController(
                                initialIndex: Hive.box('settings')
                                    .get('typeLicense', defaultValue: 0),
                                length: 2,
                                child: Container(
                                  height: 44,
                                  padding: EdgeInsets.all(4),
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: Colors.white),
                                  child: ButtonsTabBar(
                                      onTap: (i) {
                                        Hive.box('settings')
                                            .put('typeLicense', i);
                                        final answerController =
                                            Get.put(AnswerController());
                                        answerController.updateAllTests();
                                      },
                                      labelSpacing: 0,
                                      // controller: _controller2,
                                      buttonMargin: EdgeInsets.only(),
                                      backgroundColor: AppColors.purple,
                                      unselectedBackgroundColor: Colors.white,
                                      labelStyle: GoogleFonts.inter(
                                          fontSize: 16,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500),
                                      unselectedLabelStyle: GoogleFonts.inter(
                                          fontSize: 16,
                                          color: AppColors.purple,
                                          fontWeight: FontWeight.w500),
                                      contentPadding: EdgeInsets.only(
                                          top: 8,
                                          bottom: 8,
                                          left: 24,
                                          right: 24),
                                      // radius: 9,
                                      tabs: [
                                        Tab(
                                          text: "Car License",
                                        ),
                                        Tab(
                                          text: "Motorcycle License",
                                        )
                                      ]),
                                )),
                            SizedBox(
                              height: 28,
                            ),
                            Text(
                              "Select your state",
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: AppColors.gray),
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            InkWell(
                              onTap: () {
                                Get.to(() => EditStatePage());
                              },
                              child: Container(
                                padding: EdgeInsets.all(16),
                                height: 103,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    alignment: Alignment.bottomRight,
                                    image: ExactAssetImage(
                                      'assets/images/stateBackground.png',
                                    ),
                                  ),
                                  borderRadius: BorderRadius.circular(16),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      "assets/images/stateIcon.png",
                                      width: 36,
                                    ),
                                    GetBuilder<SettingsController>(
                                      builder: (_) => Text(
                                        settingsController.state.state,
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            color: AppColors.black),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 37,
                            ),
                            Text(
                              "Other",
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: AppColors.gray),
                            ),
                            SizedBox(
                              height: 17,
                            ),
                            Wrap(
                              spacing: 16,
                              runSpacing: 16,
                              children: [
                                cardElement(
                                    "to", "Terms of Use", termsOfUsePressed),
                                cardElement("pp", "Privacy Policy",
                                    privacyPolicyPressed),
                                cardElement(
                                    "support", "Support", supportFormPressed),
                                cardElement(
                                    "share", "Share the App", sharePressed),
                                cardElement("rate", "Rate the App", () {
                                  AppReview.requestReview;
                                }),
                              ],
                            ),
                            SizedBox(
                              height: 17,
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ));
  }

  Widget cardElement(String image, String text, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(16),
        height: 103,
        width: 155.w,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16), color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              "assets/images/${image}.png",
              width: 36.sp,
            ),
            Text(
              text,
              style: GoogleFonts.inter(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: AppColors.black),
            )
          ],
        ),
      ),
    );
  }
}
