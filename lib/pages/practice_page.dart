import 'package:auto_size_text/auto_size_text.dart';
import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/pages/practice_test_page.dart';
import 'package:dmv_test/pages/subscription_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

import '../config/colors.dart';
import '../models/answer.dart';
import '../shared/appbar.dart';

class PracticePage extends StatefulWidget {
  PracticePage({Key? key}) : super(key: key);

  @override
  State<PracticePage> createState() => _PracticePageState();
}

class _PracticePageState extends State<PracticePage> {
  final answerController = Get.put(AnswerController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // answerController.getPracticeWidget(init: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            bottom: false,
            top: false,
            child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
              CustomAppBar(
                  InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      child: Image.asset(
                        "assets/images/Back.png",
                        width: 24,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 37),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Practice",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: AppColors.black),
                    ),
                  ),
                  Container()),
              SizedBox(
                height: 24,
              ),
              Expanded(
                child: DefaultTabController(
                    length: 4,
                    child: Column(children: [
                      Container(
                        height: 44,
                        margin: EdgeInsets.symmetric(horizontal: 16),
                        padding: EdgeInsets.all(4),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.white),
                        child: TabBar(
                          isScrollable: true,
                          labelPadding: EdgeInsets.symmetric(horizontal: 10),
                          labelStyle: GoogleFonts.inter(
                              fontWeight: FontWeight.w500, fontSize: 16),
                          indicator: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: AppColors.purple),
                          labelColor: Colors.white,
                          unselectedLabelColor: AppColors.purple,
                          tabs: [
                            Tab(
                              text: "All",
                            ),
                            Tab(
                              text: "In progress",
                            ),
                            Tab(
                              text: "Completed",
                            ),
                            Tab(
                              text: "Not started",
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Expanded(child: GetBuilder<AnswerController>(
                        builder: (_) {
                          return TabBarView(children: [
                            SingleChildScrollView(
                              child: Container(
                                padding: EdgeInsets.only(left: 16),
                                alignment: Alignment.topLeft,
                                child: Wrap(
                                  alignment: WrapAlignment.start,
                                  spacing: 17.75,
                                  runSpacing: 17.75,
                                  children: [
                                    for (int index = 0;
                                        index < answerController.allTest.length;
                                        index++)
                                      elementPractice(
                                          test: answerController.allTest[index],
                                          index: index)
                                  ],
                                ),
                              ),
                            ),
                            GetBuilder<AnswerController>(
                              builder: (_) {
                                return SingleChildScrollView(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 16),
                                    alignment: Alignment.topLeft,
                                    child: Wrap(
                                      alignment: WrapAlignment.start,
                                      spacing: 17.75,
                                      runSpacing: 17.75,
                                      children: [
                                        for (int index = 0;
                                            index <
                                                answerController.allTest.length;
                                            index++)
                                          if (answerController
                                                  .allTest[index].testStart &&
                                              !answerController
                                                  .allTest[index].testEnd)
                                            elementPractice(
                                                test: answerController
                                                    .allTest[index],
                                                index: index)
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                            GetBuilder<AnswerController>(
                              builder: (_) {
                                return SingleChildScrollView(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 16),
                                    alignment: Alignment.topLeft,
                                    child: Wrap(
                                      alignment: WrapAlignment.start,
                                      spacing: 17.75,
                                      runSpacing: 17.75,
                                      children: [
                                        for (int index = 0;
                                            index <
                                                answerController.allTest.length;
                                            index++)
                                          if (answerController
                                              .allTest[index].testEnd)
                                            elementPractice(
                                                test: answerController
                                                    .allTest[index],
                                                index: index)
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                            GetBuilder<AnswerController>(
                              builder: (_) {
                                return SingleChildScrollView(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 16),
                                    alignment: Alignment.topLeft,
                                    child: Wrap(
                                        alignment: WrapAlignment.start,
                                        spacing: 17.75,
                                        runSpacing: 17.75,
                                        children: [
                                          for (int index = 0;
                                              index <
                                                  answerController
                                                      .allTest.length;
                                              index++)
                                            if (!answerController
                                                .allTest[index].testStart)
                                              elementPractice(
                                                  test: answerController
                                                      .allTest[index],
                                                  index: index)
                                        ]),
                                  ),
                                );
                              },
                            )
                          ]);
                        },
                      ))
                    ])),
              ),
              SizedBox(
                height: 24,
              ),
            ])));
  }

  Widget elementPractice({required Test test, required int index}) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: (_) => InkWell(
              onTap: () {
                if (answerController.subscribe.value || index < 3) {
                  test.testStart = true;
                  answerController.update();
                  if (test.testEnd) {
                    Get.to(() => PracticeTestPage(Test.clearTest(test)));
                  } else {
                    Get.to(() => PracticeTestPage(test));
                  }
                } else {
                  Get.to(() => SubscriptionPage());
                }
              },
              child: Container(
                width: 163.w,
                height: 154,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.white),
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 36,
                        width: 36,
                        child: SleekCircularSlider(
                            appearance: CircularSliderAppearance(
                                infoProperties: InfoProperties(
                                    mainLabelStyle: TextStyle(
                                        color: Color.fromRGBO(0, 0, 0, 0))),
                                startAngle: -90,
                                angleRange: 360,
                                customColors: CustomSliderColors(
                                    dotColor: Color.fromRGBO(0, 0, 0, 0),
                                    trackGradientStartAngle: 0,
                                    trackGradientEndAngle: 0,
                                    progressBarColor: test.testEnd
                                        ? AppColors.success
                                        : test.testStart
                                            ? AppColors.yellow
                                            : AppColors.purple,
                                    trackColor:
                                        Color.fromRGBO(240, 240, 240, 1)),
                                customWidths: CustomSliderWidths(
                                    shadowWidth: 0,
                                    trackWidth: 5,
                                    progressBarWidth:
                                        test.bestResult.toDouble() > 0
                                            ? 5
                                            : 0)),
                            min: 0,
                            max: test.questions.length.toDouble(),
                            initialValue: test.bestResult.toDouble()),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        "${test.questions.length} questions",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                            color: AppColors.gray),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Expanded(
                        child: AutoSizeText(
                          test.name,
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: AppColors.black),
                        ),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      (answerController.subscribe.value || index < 3)
                          ? Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                color: (test.testEnd
                                        ? AppColors.success
                                        : test.testStart
                                            ? AppColors.yellow
                                            : AppColors.purple)
                                    .withOpacity(0.2),
                              ),
                              child: Text(
                                test.testEnd
                                    ? "Completed"
                                    : test.testStart
                                        ? "InProgress"
                                        : "Not Started",
                                style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: test.testEnd
                                      ? AppColors.success
                                      : test.testStart
                                          ? AppColors.yellow
                                          : AppColors.purple,
                                ),
                              ),
                            )
                          : Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                color: (AppColors.red).withOpacity(0.2),
                              ),
                              child: Text(
                                "Locked",
                                style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: AppColors.red,
                                ),
                              ),
                            )
                    ],
                  ),
                ),
              ),
            ));
  }
}
