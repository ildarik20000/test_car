import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/answer_controller.dart';
import 'package:dmv_test/controllers/timer_controller.dart';
import 'package:dmv_test/pages/practice_result_page.dart';

import 'package:dmv_test/shared/appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:google_fonts/google_fonts.dart';

import '../models/answer.dart';

class QuestionsTestPage extends StatefulWidget {
  int selectQuestion = 0;
  late List<Question> test;
  late List<int> indexTest;
  int type;
  String title;
  QuestionsTestPage(this.type, this.title,
      {required this.selectQuestion, Key? key})
      : super(key: key);

  @override
  State<QuestionsTestPage> createState() => _QuestionsTestPageState();
}

class _QuestionsTestPageState extends State<QuestionsTestPage> {
  late ScrollController scrollController;
  final answerController = Get.put(AnswerController());

  @override
  void initState() {
    widget.test = widget.type == 0
        ? answerController.allQuestion
        : widget.type == 1
            ? answerController.correctQuestion
            : widget.type == 2
                ? answerController.wrongQuestion
                : answerController.skippedQuestion;
    widget.indexTest = widget.type == 0
        ? answerController.allIndex
        : widget.type == 1
            ? answerController.correctIndex
            : widget.type == 2
                ? answerController.wrongIndex
                : answerController.skippedIndex;
    double _position = widget.selectQuestion * 63;
    scrollController = ScrollController(initialScrollOffset: _position);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: SafeArea(
        top: false,
        child: Column(
          children: [
            CustomAppBar(
                InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                    child: Image.asset(
                      "assets/images/Close.png",
                      height: 24,
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    widget.title,
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        color: AppColors.black),
                  ),
                ),
                Container()),
            SizedBox(
              height: 12,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: DefaultTabController(
                  length: widget.test.length,
                  child: Column(
                    children: [
                      Container(
                        height: 44,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            controller: scrollController,
                            itemCount: widget.test.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                hoverColor: Colors.white.withOpacity(0),
                                splashColor: Colors.white.withOpacity(0),
                                focusColor: Colors.white.withOpacity(0),
                                highlightColor: Colors.white.withOpacity(0),
                                onTap: () {
                                  widget.selectQuestion = index;
                                  setState(() {});
                                },
                                child: tabBarWidget(
                                    widget.indexTest[index],
                                    index == widget.selectQuestion,
                                    widget.test[index],
                                    index > widget.selectQuestion),
                              );
                            }),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          widget.test[widget.selectQuestion].title,
                          textAlign: TextAlign.start,
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: AppColors.black),
                        ),
                      ),
                      if (widget.test[widget.selectQuestion].image.isNotEmpty)
                        Container(
                            margin: EdgeInsets.only(top: 12),
                            height: 152,
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(vertical: 16),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12)),
                            child: SvgPicture.asset(
                              "assets/svg/" +
                                  widget.test[widget.selectQuestion].image,
                            )),
                      SizedBox(
                        height: 24,
                      ),
                      Expanded(
                          child: ListView.builder(
                              itemCount: widget
                                  .test[widget.selectQuestion].answers.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  padding: EdgeInsets.all(18),
                                  margin: EdgeInsets.only(bottom: 12),
                                  // height: 56,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(16)),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        "assets/images/${getCheckbox(index)}.png",
                                        width: 24,
                                      ),
                                      SizedBox(
                                        width: 18,
                                      ),
                                      Expanded(
                                        child: Text(
                                          widget.test[widget.selectQuestion]
                                              .answers[index].text,
                                          style: GoogleFonts.inter(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 16,
                                              color: AppColors.black),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              })),
                    ],
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                if (widget.selectQuestion + 1 == widget.test.length) {
                  Get.back();
                } else {
                  widget.selectQuestion++;
                }
                setState(() {});
                double _position = widget.selectQuestion * 63;
                if (_position > scrollController.position.maxScrollExtent)
                  _position = scrollController.position.maxScrollExtent;
                scrollController.animateTo(_position,
                    duration: Duration(milliseconds: 200),
                    curve: Curves.linear);
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                width: double.infinity,
                height: 56,
                decoration: BoxDecoration(
                    color: AppColors.purple,
                    borderRadius: BorderRadius.circular(16)),
                child: Center(
                  child: Text(
                    "Next",
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: Colors.white),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget tabBarWidget(int index, bool select, Question test, bool ahead) {
    return Container(
        margin: EdgeInsets.only(right: 20),
        width: 44,
        height: 44,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: getBackColor(test, select)),
        child: Center(
          child: Text(
            (index + 1).toString(),
            style: GoogleFonts.inter(
              fontWeight: FontWeight.w500,
              fontSize: 20,
              color: select ? Colors.white : test.colorText,
            ),
          ),
        ));
  }

  Color getBackColor(Question test, bool select) {
    if (select) {
      if (!test.editExam) {
        if (test.colorContainer == AppColors.red.withOpacity(0.2)) {
          return AppColors.red;
        }
        if (test.colorContainer == AppColors.success.withOpacity(0.2)) {
          return AppColors.success;
        }
      }
      return AppColors.purple;
    } else {
      return test.colorContainer;
    }
  }

  String getCheckbox(int index) {
    if (widget.test[widget.selectQuestion].selectAnswerId ==
            widget.test[widget.selectQuestion].correntAnswerId &&
        widget.test[widget.selectQuestion].selectAnswerId == index) {
      return "CheckboxTrue";
    }
    if (widget.test[widget.selectQuestion].selectAnswerId !=
            widget.test[widget.selectQuestion].correntAnswerId &&
        widget.test[widget.selectQuestion].selectAnswerId == index) {
      return "CheckboxFalse";
    }
    if (index == widget.test[widget.selectQuestion].correntAnswerId) {
      return "CheckboxTrue";
    }
    return "CheckboxNotSelect";
  }
}
