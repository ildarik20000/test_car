import 'package:dmv_test/config/colors.dart';
import 'package:dmv_test/controllers/settings_controller.dart';
import 'package:dmv_test/models/states.dart';
import 'package:dmv_test/pages/main_page.dart';
import 'package:dmv_test/pages/subscription_page.dart';
import 'package:dmv_test/services/boxes.dart';
import 'package:dmv_test/shared/appbar.dart';
import 'package:dmv_test/shared/background_onboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';

import '../controllers/answer_controller.dart';

class EditStatePage extends StatefulWidget {
  int tap = -1;
  @override
  State<EditStatePage> createState() => _EditStatePageState();
}

class _EditStatePageState extends State<EditStatePage> {
  List<StateModel> searcList = [];

  final settingsController = Get.put(SettingsController());
  @override
  void initState() {
    // TODO: implement initState
    searcList.addAll(states);
    for (int i = 0; i < searcList.length; i++) {
      if (searcList[i].state == settingsController.state.state) {
        searcList.removeAt(i);
        break;
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          FocusManager.instance.primaryFocus?.unfocus();
        });
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          bottom: false,
          top: false,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomAppBar(
                  InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      child: Image.asset(
                        "assets/images/Back.png",
                        width: 24,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 37),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "State selection",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: AppColors.black),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      settingsController.state = searcList[widget.tap];
                      settingsController.update();
                      Boxes.getState().add(searcList[widget.tap]);
                      final answerController = Get.put(AnswerController());
                      answerController.updateAllTests();
                      Get.back();
                    },
                    child: Container(
                      child: Text(
                        "Save",
                        style: GoogleFonts.inter(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: AppColors.purple),
                      ),
                    ),
                  )),
              SizedBox(
                height: 24,
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                            offset: Offset(0, -5),
                            blurRadius: 15)
                      ],
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(24),
                          topRight: Radius.circular(24))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 16,
                      ),
                      Container(
                        height: 48,
                        width: double.infinity,
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 14),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                            color: Color.fromRGBO(240, 240, 240, 1)),
                        child: Row(
                          children: [
                            Expanded(
                                child: TextField(
                              onChanged: (query) {
                                filterSearchResults(query);
                              },
                              cursorColor: AppColors.purple,
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.only(top: 0, bottom: 10),
                                  hintText: "Search",
                                  hintStyle: GoogleFonts.inter(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16,
                                      color: AppColors.gray),
                                  border: InputBorder.none),
                            )),
                            Image.asset(
                              "assets/images/search.png",
                              width: 24,
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Your state",
                          textAlign: TextAlign.start,
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: AppColors.gray),
                        ),
                      ),
                      SizedBox(
                        height: 9,
                      ),
                      _statesElement(
                          settingsController.state.state, widget.tap == -1),
                      SizedBox(
                        height: 21,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Available states",
                          textAlign: TextAlign.start,
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: AppColors.gray),
                        ),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Expanded(
                        child: searcList.isNotEmpty
                            ? ListView.builder(
                                itemCount: searcList.length,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      setState(() {
                                        widget.tap = index;
                                      });
                                    },
                                    child: _statesElement(
                                        searcList[index].state,
                                        widget.tap == index),
                                  );
                                })
                            : Center(
                                child: Text(
                                  "Not found state",
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16,
                                      color: AppColors.gray),
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _statesElement(String text, bool enabled) {
    return Container(
      height: 48,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text,
            style: GoogleFonts.inter(
                fontWeight: FontWeight.w500,
                fontSize: 16,
                color: AppColors.black),
          ),
          Container(
            width: 20,
            height: 20,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: enabled
                    ? Color.fromRGBO(219, 212, 241, 1)
                    : Color.fromRGBO(0, 0, 0, 0.1)),
            child: enabled
                ? Center(
                    child: Image.asset(
                    "assets/images/onboarding3_3.png",
                    color: AppColors.purple,
                    width: 10,
                  ))
                : Container(),
          )
        ],
      ),
    );
  }

  void filterSearchResults(String query) {
    List<StateModel> dummySearchList = [];
    dummySearchList.addAll(states);
    for (int i = 0; i < dummySearchList.length; i++) {
      if (dummySearchList[i].state == settingsController.state.state) {
        dummySearchList.removeAt(i);
        break;
      }
    }
    if (query.isNotEmpty) {
      List<StateModel> dummyListData = [];
      dummySearchList.forEach((item) {
        if (item.state.toLowerCase().startsWith(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        searcList.clear();
        searcList.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        searcList.clear();
        searcList.addAll(states);
      });
    }
  }
}
