import 'package:auto_size_text/auto_size_text.dart';
import 'package:dmv_test/models/history.dart';
import 'package:dmv_test/pages/exam_result_page.dart';
import 'package:dmv_test/pages/practice_page.dart';
import 'package:dmv_test/pages/practice_result_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

import '../config/colors.dart';
import '../models/answer.dart';

class HistoryController extends GetxController {
  List<History> historys = <History>[].obs;
  List<int> selectListDelete = <int>[].obs;
  RxBool selectedDelete = false.obs;
  // List<Widget> historysWidget = <Widget>[].obs;

  selectedAll() {
    selectListDelete.clear();
    for (int index = 0; index < historys.length; index++) {
      selectListDelete.add(index);
    }
    update();
  }

  save() {
    selectedDelete.value = false;
    selectListDelete.clear();
    update();
  }

  addElement(History history) {
    historys.insert(0, history);
  }

  deleteOneHistory(int index) {
    historys.removeAt(index);
    update();
  }

  deleteHistory() {
    for (int i = selectListDelete.length - 1; i >= 0; i--) {
      historys.removeAt(selectListDelete[i]);
    }
    selectListDelete.clear();
    update();
  }
}
