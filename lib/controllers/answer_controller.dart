import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:dmv_test/controllers/settings_controller.dart';
import 'package:dmv_test/pages/subscription_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

import '../config/colors.dart';
import '../models/answer.dart';
import 'package:flutter/services.dart' show rootBundle;

import '../pages/practice_test_page.dart';

class AnswerController extends GetxController {
  List<Widget> allElements = <Widget>[].obs;
  List<Widget> correctElements = <Widget>[].obs;
  List<Widget> wrongElements = <Widget>[].obs;
  List<Question> allQuestion = <Question>[].obs;
  List<Question> correctQuestion = <Question>[].obs;
  List<Question> wrongQuestion = <Question>[].obs;
  List<Question> skippedQuestion = [];
  List<Widget> skippedElements = [];
  List<int> skippedIndex = [];

  List<int> allIndex = <int>[].obs;
  List<int> correctIndex = <int>[].obs;
  List<int> wrongIndex = <int>[].obs;
  List<Test> allTest = <Test>[].obs;
  late Test examTest;
  RxBool subscribe = false.obs;

  updateAllTests() async {
    final settingsController = Get.put(SettingsController());
    Future<String> getFileData(String path) async {
      return await rootBundle.loadString(path);
    }

    String nameState = settingsController.state.state
        .toLowerCase()
        .replaceAll(RegExp(r' '), "-");
    final questions = questionsFromJson(
        await getFileData("assets/questions/${nameState}.json"));
    allTest.clear();
    String typeTest =
        Hive.box('settings').get('typeLicense', defaultValue: 0) == 0
            ? "practice-test"
            : "motorcycle-test";
    allTest.addAll(questions.tests[typeTest]!);

    update();
  }

  int getQuestionsCompleted() {
    int all = 0;
    for (int i = 0; i < allTest.length; i++) {
      all += allTest[i].bestResult;
    }
    return all;
  }

  int getAllQuestions() {
    int all = 0;

    for (int i = 0; i < allTest.length; i++) {
      all += allTest[i].questions.length;
    }
    return all;
  }

  

  generatedExam() {
    final settingsController = Get.put(SettingsController());
    var rng = Random();
    examTest = Test(id: "0", name: "Exam", questions: []);
    for (int i = 0; i < settingsController.state.question; i++) {
      Question question = Question.copyWith(allTest[rng.nextInt(allTest.length)]
              .questions[
          rng.nextInt(allTest[rng.nextInt(allTest.length)].questions.length)]);
      examTest.questions.add(question);
    }
  }

  int getTrueAnswer(Test test) {
    int out = 0;
    for (int i = 0; i < test.questions.length; i++) {
      if (test.questions[i].selectAnswerId ==
          test.questions[i].correntAnswerId) {
        out++;
      }
    }
    return out;
  }

  
}
