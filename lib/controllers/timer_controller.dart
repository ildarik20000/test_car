import 'dart:async';

import 'package:get/get.dart';

import '../pages/exam_result_page.dart';
import 'answer_controller.dart';

class TimerController extends GetxController {
  var time = 0.obs;
  late Timer timer;
  String getTimeString() {
    int hours = time.value ~/ 60;
    int minutes = time.value % 60;
    return (hours < 10 ? "0${hours.toString()}" : hours.toString()) +
        ":" +
        (minutes < 10 ? "0${minutes.toString()}" : minutes.toString());
  }

  startTimer(int timeDelation) {
    time.value = timeDelation;
    updateTime();
  }

  stopTimer() {
    timer.cancel();
  }

  updateTime() {
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (time > 0) {
        time.value--;
        update();
      } else {
        final answerController = Get.put(AnswerController());
        Get.to(() => ExamResultPage(answerController.examTest));
        timer.cancel();
      }
    });
  }
}
