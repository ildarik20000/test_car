import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Trans;

import '../services/save_and_download.dart';

class NavigationBarController extends GetxController
    {
  var tabIndex = 1.obs;
  var devicesPage = 0.obs;
  var settingsPage = 0.obs;
  var articlesPage = 0.obs;

  int get pageIndex => tabIndex.value;

 



  void changeTabIndex(int index) {
    if (index == 0) {
      // final onboardingController = Get.put(OnboardingController());

      tabIndex.value = index;
    }
    if (index == 1) {
      tabIndex.value = index;
    }
    if (index == 2) {
      tabIndex.value = index;
    }
    update();
  }
}
